# Project name: Organizations Manager

## Description:
Egy állami megbízást kapott a cég az egyes egyesületek és azok  ingóságaik nyílvántartására kell egy rendszert kiépiteni.
## Function List:
- Az szervezethez tartózó Személyek Listázása / Hozzáadása / módosítása / törlése
- Az egyes szervezetek Listázása / Hozzáadása / módosítása / törlése
- Az egyes szervezetekhez tartozó járművek Listázása / Hozzáadása / módosítása / törlése
- Az egyes szervezetekhez tarozó ingatlanok Listázása / Hozzáadása / módosítása / törlése 
- Lehetőség az egyes szervezetek által birtokolt járművek összes megtett kilóméterének listázására
- Lehetőség az egyes szervezeteket csoportsítva összesíteni a vagyonukat
- Lehetőség az egyes szervezethez tartozó személyek átlagos életkorának kilistázására
- JAVA végpontnak GET requesten átadott jármű megtett kilómétere / ingatlan mérete alapján egy "árajánlatot" kérni hogy mennyiért lehetne eladni az adott ingóságot 

