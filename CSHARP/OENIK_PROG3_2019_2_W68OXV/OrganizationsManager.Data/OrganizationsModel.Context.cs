﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OrganizationsManager.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    /// <summary>
    /// Provides methods for database manipulations
    /// </summary>
    public partial class OrganizationsContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrganizationsContext"/> class.
        /// </summary>
        public OrganizationsContext()
            : base("name=OrganizationsContext")
        {
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    /// <summary>
    /// Allows access to the Organization Table
    /// </summary>
        public virtual DbSet<Organization> Organization { get; set; }
        /// <summary>
        /// Allows access to the Person table.
        /// </summary>
        public virtual DbSet<Person> Person { get; set; }
        /// <summary>
        /// Allows access to the Property table.
        /// </summary>
        public virtual DbSet<Property> Property { get; set; }
        /// <summary>
        /// Allows access to the Vehicle table.
        /// </summary>
        public virtual DbSet<Vehicle> Vehicle { get; set; }
    }
}
