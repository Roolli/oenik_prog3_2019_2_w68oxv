﻿CREATE TABLE Person
(
   Id int NOT NULL PRIMARY KEY,
    OrganizationID int,
    PersonName NVARCHAR(64) NOT NULL,
    DateOfBirth DATE NOT NULL,
    Gender int NOT NULL,
    City NVARCHAR(48) NOT NULL,
    Email NVARCHAR(64)
);



CREATE TABLE Organization
(
    Id int NOT NULL PRIMARY KEY,
    MemberCount int NOT NULL,
    Vault int NOT NULL,
    GroupType nVARCHAR(32) NOT NULL,
    Name NVARCHAR(128) NOT NULL,
	FoundationDate DATE NOT NULL
);


CREATE TABLE Vehicle
(
    LicensePlate NVARCHAR(7) NOT NULL PRIMARY KEY,
    Make NVARCHAR(32) NOT NULL,
    Model NVARCHAR(32) NOT NULL,
    IsRental int NOT NULL,
     Color NVARCHAR(7) NOT NULL,
    AquireDate DATE NOT NULL,
	DistanceTravelled int NOT NULL,
    OwnerId int NOT NULL FOREIGN key REFERENCES Organization(Id)
);
CREATE TABLE Property
(
    Id int NOT NULL PRIMARY KEY,
    BuildingSize int NOT NULL,
    IsRental int NOT NULL,
    City NVARCHAR(48) NOT NULL,
    PropertyAddress NVARCHAR(128) NOT NULL,
    AquireDate DATE NOT NULL,
    OwnerID int NOT NULL FOREIGN KEY REFERENCES Organization (Id)
);

ALTER TABLE Person ADD CONSTRAINT person_fk  FOREIGN key  (OrganizationId)  REFERENCES Organization(Id);


