﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides methods that all logic classes must implement.
    /// </summary>
    /// <typeparam name="T"> Typeof Entity to provide Logic for.</typeparam>
    public interface ILogic<T>
         where T : class
    {
        /// <summary>
        /// Adds an entity to the database.
        /// </summary>
        /// <param name="entity">Entity to add.</param>
        void Add(T entity);

        /// <summary>
        /// Gets all the entities in the database ready to be output to the UI.
        /// </summary>
        /// <returns>A collection of entities.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Gets a single entity whose key matches the supplied one.
        /// </summary>
        /// <param name="primaryKey">Id of the entity to get.</param>
        /// <returns>The entity's string representation or an empty string if not found.</returns>
        T Get(params object[] primaryKey);

        /// <summary>
        /// Updates the given entity with the supplied one's data.
        /// </summary>
        /// <param name="key">Id of the entity to update.</param>
        /// <param name="data">Data to update for the given entity.</param>
        /// <returns>True on success, false otherwise.</returns>
        bool Update(object key, T data);

        /// <summary>
        /// Deletes the entity with the provided key.
        /// </summary>
        /// <param name="key">To be deleted entity's key.</param>
        /// <returns>True on success, false otherwise.</returns>
        bool Delete(object key);
    }
}
