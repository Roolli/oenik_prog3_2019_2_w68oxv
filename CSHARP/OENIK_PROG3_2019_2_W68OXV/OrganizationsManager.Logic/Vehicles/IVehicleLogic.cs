﻿// <copyright file="IVehicleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Vehicles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Defines all the logic for the vehicle Entity.
    /// </summary>
    public interface IVehicleLogic : ILogic<Vehicle>
    {
        /// <summary>
        /// Gets the total distance travelled by each organization's vehicles.
        /// </summary>
        /// <returns>A collection of strings containing the organization's name and their total distance travelled.</returns>
        IEnumerable<string> GetTotalDistance();
    }
}
