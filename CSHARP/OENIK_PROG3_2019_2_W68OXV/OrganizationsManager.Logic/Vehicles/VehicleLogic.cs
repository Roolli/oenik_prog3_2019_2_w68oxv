﻿// <copyright file="VehicleLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Vehicles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository;
    using OrganizationsManager.Repository.Vehicles;

    /// <summary>
    /// Handles all logic regarding the vehicle entity.
    /// </summary>
    internal class VehicleLogic : Logic<Vehicle>, IVehicleLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleLogic"/> class.
        /// </summary>
        /// <param name="repository">The repository to use.</param>
        public VehicleLogic(IVehicleRepository repository)
            : base(repository)
        {
        }

        /// <inheritdoc/>
        public IEnumerable<string> GetTotalDistance() => (this.Repository as IVehicleRepository).GetGroupedVehicleDistances().ToList().Select(v => v.ToString());
    }
}
