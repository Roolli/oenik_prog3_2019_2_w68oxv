﻿// <copyright file="IPersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Person logic.
    /// </summary>
    public interface IPersonLogic : ILogic<Person>
    {
        /// <summary>
        /// Gets the average age for every organization.
        /// </summary>
        /// <returns> A collection containing the age and the organization's name. </returns>
        IEnumerable<string> GetAveragePersonAge();
    }
}
