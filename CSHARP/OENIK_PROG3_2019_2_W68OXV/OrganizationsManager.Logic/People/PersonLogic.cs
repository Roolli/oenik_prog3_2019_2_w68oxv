﻿// <copyright file="PersonLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository;
    using OrganizationsManager.Repository.People;

    /// <summary>
    /// The logic class for managing the Person entity related methods.
    /// </summary>
    internal class PersonLogic : Logic<Person>, IPersonLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonLogic"/> class.
        /// </summary>
        /// <param name="repository"> The repository to use. </param>
        public PersonLogic(IPersonRepository repository)
            : base(repository)
        {
        }

        /// <inheritdoc/>
        public IEnumerable<string> GetAveragePersonAge() => (this.Repository as IPersonRepository).GetAveragePeopleAgePerOrganization().Select(v => v.ToString());
    }
}
