﻿// <copyright file="ILogicFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using OrganizationsManager.Logic.Buildings;
    using OrganizationsManager.Logic.JavaInteraction;
    using OrganizationsManager.Logic.Vehicles;
    using OrganizationsManager.Repository.Organizations;
    using OrganizationsManager.Repository.People;

    /// <summary>
    /// Provides all the required methods for the logicfactory.
    /// </summary>
    public interface ILogicFactory
    {
        /// <summary>
        /// Creates a new Logic class which handles all Organization logic.
        /// </summary>
        /// <returns>a new instance of <see cref="OrganizationLogic"/>.</returns>
        IOrganizationLogic GetOrganizationLogic();

        /// <summary>
        /// Creates a new Logic class which handles all  Person related logic.
        /// </summary>
        /// <returns>a new instance of <see cref="PersonLogic"/>.</returns>
        IPersonLogic GetPersonLogic();

        /// <summary>
        /// Creates a new Logic class which handles all Vehicle related logic.
        /// </summary>
        /// <returns> a new instance of <see cref="VehicleLogic"/>.</returns>
        IVehicleLogic GetVehicleLogic();

        /// <summary>
        /// Creates a new logic class which handles all property related logic.
        /// </summary>
        /// <returns> a new instance of <see cref="PropertyLogic"/>.</returns>
        IPropertyLogic GetPropertyLogic();

        /// <summary>
        /// Creates a new logic class which handles all java interaction logic.
        /// </summary>
        /// <returns>A new instance of <see cref="JavaLogic"/>.</returns>
        IJavaLogic GetJavaLogic();
    }
}