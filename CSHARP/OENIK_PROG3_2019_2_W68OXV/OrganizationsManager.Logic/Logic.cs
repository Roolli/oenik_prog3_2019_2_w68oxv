﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Repository;

    /// <summary>
    /// Base class for all common Logic (CRUD).
    /// </summary>
    /// <typeparam name="T">Type of entity to create the logic for.</typeparam>
    public abstract class Logic<T> : ILogic<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logic{T}"/> class.
        /// </summary>
        /// <param name="repository">The repository to use.</param>
        protected Logic(IRepository<T> repository) => this.Repository = repository;

        /// <summary>
        /// Gets the repository which this class uses.
        /// </summary>
        protected IRepository<T> Repository { get; private set; }

        /// <inheritdoc/>
        public void Add(T entity) => this.Repository.Add(entity);

        /// <inheritdoc/>
        public bool Delete(object key) => this.Repository.Delete(key);

        /// <inheritdoc/>
        public T Get(params object[] keyValues) => this.Repository.GetOne(keyValues);

        /// <inheritdoc/>
        public IEnumerable<T> GetAll() => this.Repository.GetAll().ToList();

        /// <inheritdoc/>
        public bool Update(object key, T entity) => this.Repository.Update(entity, key);
    }
}
