﻿// <copyright file="IJavaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.JavaInteraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Defines all java logic.
    /// </summary>
    public interface IJavaLogic
    {
        /// <summary>
        /// Sends the vehicle's details to the server for sale estimation.
        /// </summary>
        /// <param name="veh">The instance to send.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<string> GetEstimatedVehicleValue(List<VehicleDTO> veh);

        /// <summary>
        /// Sends the property's details to the server for sale estimation.
        /// </summary>
        /// <param name="property">the instance to send.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<string> GetEstimatedPropertyValue(List<PropertyDTO> property);
    }
}
