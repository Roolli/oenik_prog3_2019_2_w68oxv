﻿// <copyright file="VehicleDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.JavaInteraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using OrganizationsManager.Data;

    /// <summary>
    /// Vehicle data transfer object for easy serialization.
    /// </summary>
    public class VehicleDTO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleDTO"/> class.
        /// </summary>
        /// <param name="veh">Vehicle to serialize.</param>
        public VehicleDTO(Vehicle veh)
        {
            this.Make = veh.Make;
            this.Model = veh.Model;
            this.DistanceTravelled = veh.DistanceTravelled;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VehicleDTO"/> class.
        /// </summary>
        public VehicleDTO()
        {
        }

        /// <summary>
        /// Gets or sets the make of the vehicle.
        /// </summary>
        public string Make { get; set; }

        /// <summary>
        /// Gets or sets the model of the vehicle.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Gets or sets the distance travelled by the vehicle.
        /// </summary>
        public int DistanceTravelled { get; set; }

        /// <summary>
        ///  Allows quick object creation for serialization purposes.
        /// </summary>
        /// <param name="veh">the object to serialize.</param>
        public static implicit operator VehicleDTO(Vehicle veh)
        {
            return new VehicleDTO
            {
                DistanceTravelled = veh.DistanceTravelled,
                Model = veh.Model,
                Make = veh.Make,
            };
        }
    }
}
