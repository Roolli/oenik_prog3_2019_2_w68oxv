﻿// <copyright file="JavaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.JavaInteraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using OrganizationsManager.Data;

    /// <summary>
    /// Handles all logic related to the java endpoint.
    /// </summary>
    internal class JavaLogic : IJavaLogic
    {
        private readonly HttpClient httpClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaLogic"/> class.
        /// </summary>
        /// <param name="httpClient">The http client to use.</param>
        public JavaLogic(HttpClient httpClient) => this.httpClient = httpClient;

        /// <inheritdoc/>
        public async Task<string> GetEstimatedPropertyValue(List<PropertyDTO> property)
        {
            var obj = JsonConvert.SerializeObject(property);
            var builder = new UriBuilder("http://localhost:8080/OrganizationManager")
            {
                // using this lowbudget method to avoid using System.Web.HttpUtility
                Query = $"PropertyJson={obj}",
            };
            string result = await this.SendRequest(builder.ToString());
            return result;
        }

        /// <inheritdoc/>
        public async Task<string> GetEstimatedVehicleValue(List<VehicleDTO> veh)
        {
            var obj = JsonConvert.SerializeObject(veh);
            var builder = new UriBuilder("http://localhost:8080/OrganizationManager")
            {
                Query = $"VehicleJson={obj}",
            };
            string result = await this.SendRequest(builder.ToString());
            return result;
        }

        private async Task<string> SendRequest(string uri)
        {
            try
            {
                var response = await this.httpClient.GetAsync(uri);
                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return "Something went wrong :/";
                }

                string content = await response.Content.ReadAsStringAsync();
                var contentTemplate = new { res = new string[0] };
                string[] values = JsonConvert.DeserializeAnonymousType(content, contentTemplate).res;
                return string.Join(Environment.NewLine, values);
            }
            catch (Exception)
            {
                return "something went wrong :/";
            }
        }
    }
}
