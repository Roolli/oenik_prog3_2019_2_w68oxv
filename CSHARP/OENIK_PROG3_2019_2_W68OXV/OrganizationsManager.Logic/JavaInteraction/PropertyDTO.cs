﻿// <copyright file="PropertyDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.JavaInteraction
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Property Data transfer object for easy serialization.
    /// </summary>
    public class PropertyDTO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyDTO"/> class.
        /// </summary>
        /// <param name="prop"> property to convert.</param>
        public PropertyDTO(Property prop)
        {
            this.BuildingSize = prop.BuildingSize;
            this.City = prop.City;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyDTO"/> class.
        /// </summary>
        public PropertyDTO()
        {
        }

        /// <summary>
        /// Gets or sets the building's size.
        /// </summary>
        public int BuildingSize { get; set; }

        /// <summary>
        /// Gets or sets the city the building is in.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Allows quick object creation for serialization purposes.
        /// </summary>
        /// <param name="property">the object to serialize.</param>
        public static implicit operator PropertyDTO(Property property)
        {
            return new PropertyDTO
            { BuildingSize = property.BuildingSize, City = property.City };
        }
    }
}
