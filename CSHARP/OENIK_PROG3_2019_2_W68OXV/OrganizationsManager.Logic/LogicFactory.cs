﻿// <copyright file="LogicFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Logic.Buildings;
    using OrganizationsManager.Logic.JavaInteraction;
    using OrganizationsManager.Logic.Vehicles;
    using OrganizationsManager.Repository.Organizations;
    using OrganizationsManager.Repository.People;

    /// <summary>
    /// A factory to create logic classes.
    /// </summary>
    public class LogicFactory : ILogicFactory
    {
        private readonly Repository.RepositoryFactory repositoryFactory = new Repository.RepositoryFactory();

        /// <inheritdoc/>
        public IOrganizationLogic GetOrganizationLogic() => new OrganizationLogic(this.repositoryFactory.GetOrganizationRepository());

        /// <inheritdoc/>
        public IPersonLogic GetPersonLogic() => new PersonLogic(this.repositoryFactory.GetPersonRepository());

        /// <inheritdoc/>
        public IPropertyLogic GetPropertyLogic() => new PropertyLogic(this.repositoryFactory.GetPropertyRepository());

        /// <inheritdoc/>
        public IVehicleLogic GetVehicleLogic() => new VehicleLogic(this.repositoryFactory.GetVehicleRepository());

        /// <inheritdoc/>
        public IJavaLogic GetJavaLogic() => new JavaLogic(new System.Net.Http.HttpClient());
    }
}
