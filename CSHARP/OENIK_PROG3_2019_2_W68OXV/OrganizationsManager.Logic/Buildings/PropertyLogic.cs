﻿// <copyright file="PropertyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Buildings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository;
    using OrganizationsManager.Repository.Buildings;

    /// <summary>
    /// Property logic class.
    /// </summary>
    internal class PropertyLogic : Logic<Property>, IPropertyLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyLogic"/> class.
        /// </summary>
        /// <param name="repository">The repository to use.</param>
        public PropertyLogic(IPropertyRepository repository)
            : base(repository)
        {
        }
    }
}
