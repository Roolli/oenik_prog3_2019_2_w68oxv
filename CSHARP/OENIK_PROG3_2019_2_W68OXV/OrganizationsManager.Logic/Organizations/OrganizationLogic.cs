﻿// <copyright file="OrganizationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("OrganizationsManager.Logic.Tests")]

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository;
    using OrganizationsManager.Repository.Organizations;

    /// <summary>
    /// Organization Logic class.
    /// </summary>
    internal class OrganizationLogic : Logic<Organization>, IOrganizationLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrganizationLogic"/> class.
        /// </summary>
        /// <param name="repository">The repository which this Logic handles.</param>
        public OrganizationLogic(IOrganizationRepository repository)
            : base(repository)
        {
        }

        /// <inheritdoc/>
        public IEnumerable<string> GetGroupedWealths() => (this.Repository as IOrganizationRepository).GetGroupedWealths().AsEnumerable().Select(v => v.ToString());
    }
}
