﻿// <copyright file="IOrganizationLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository;

    /// <summary>
    /// Organization Logic class.
    /// </summary>
    public interface IOrganizationLogic : ILogic<Organization>
    {
        /// <summary>
        /// Gets the combined wealth based on organization type.
        /// </summary>
        /// <returns> A collection of strings containing the groupType and the value.</returns>
        IEnumerable<string> GetGroupedWealths();
    }
}
