﻿// <copyright file="EntityMenuAction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    /// <summary>
    /// Defines set menuActions for the entity menus.
    /// </summary>
    internal enum EntityMenuAction
    {
        /// <summary>
        /// Add action
        /// </summary>
        Add,

        /// <summary>
        /// List action
        /// </summary>
        List,

        /// <summary>
        /// Update Action
        /// </summary>
        Update,

        /// <summary>
        /// Delete Action
        /// </summary>
        Delete,

        /// <summary>
        /// Back action
        /// </summary>
        Back,
    }
}
