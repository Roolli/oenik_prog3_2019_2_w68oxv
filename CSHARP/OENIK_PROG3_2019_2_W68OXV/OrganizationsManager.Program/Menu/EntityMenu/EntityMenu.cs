﻿// <copyright file="EntityMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using OrganizationsManager.Logic;

    /// <summary>
    /// Menu for handling Entities.
    /// </summary>
    /// <typeparam name="T"> Entity model classes for basic CRUD.</typeparam>
    internal class EntityMenu<T> : Menu
        where T : class
    {
        private readonly ILogic<T> logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityMenu{T}"/> class.
        /// </summary>
        /// <param name="logic">The logic class to use.</param>
        public EntityMenu(ILogic<T> logic)
            : base($"Actions for entity ({typeof(T).Name}):", Enum.GetNames(typeof(EntityMenuAction))) => this.logic = logic;

        /// <inheritdoc/>
        protected override void PerformAction(int currentAction)
        {
            switch ((EntityMenuAction)currentAction)
            {
                case EntityMenuAction.Add:
                    this.PerformAdd();
                    break;
                case EntityMenuAction.List:
                    this.OutputResult(this.logic.GetAll().Select(l => l.ToString()));
                    break;
                case EntityMenuAction.Update:
                    this.PerformUpdate();
                    break;
                case EntityMenuAction.Delete:
                    this.PerformDelete();
                    break;
                case EntityMenuAction.Back:
                    this.CloseMenu();
                    break;
            }
        }

        private void PerformDelete()
        {
            T entityToDelete = this.GetEntityByID(out object id);
            if (entityToDelete is null)
            {
                Console.WriteLine("Invalid ID specified.");
                Console.ReadKey();
                return;
            }

            this.DisplayResultMessage(this.logic.Delete(id));
        }

        /// <summary>
        /// Gets one entity whose ID is the supplied one.
        /// </summary>
        /// <returns> The entity if found, null otherwise.</returns>
        private T GetEntityByID(out object primaryKey)
        {
            primaryKey = null;
            bool success = false;
            while (!success)
            {
                Console.Clear();
                var propType = typeof(T).GetProperties().First(v => v.GetCustomAttribute<KeyAttribute>() != null).PropertyType;
                Console.Write($"Please provide an entity Primary key:({propType}): ");
                string input = Console.ReadLine();
                try
                {
                    primaryKey = Convert.ChangeType(input, propType, CultureInfo.InvariantCulture);
                    T result = this.logic.Get(primaryKey);
                    return result;
                }
                catch (Exception)
                {
                    continue;
                }
            }

            return default(T);
        }

        private void PerformUpdate()
        {
            T entityToUpdate = this.GetEntityByID(out object entityID);
            if (entityToUpdate is null)
            {
                Console.WriteLine("Invalid ID specified.");
                Console.ReadKey();
                return;
            }

            if (this.ModifyObject(entityToUpdate))
            {
                this.DisplayResultMessage(this.logic.Update(entityID, entityToUpdate));
            }
        }

        private void DisplayResultMessage(bool success)
        {
            Console.WriteLine(success ? "Action completed successfully!" : "something went wrong :/");
            Console.ReadKey();
        }

        /// <summary>
        /// Modifies the passed in object.
        /// </summary>
        /// <param name="instance"> the instance to modify.</param>
        /// <returns>True if completed, false if cancelled. </returns>
        private bool ModifyObject(T instance)
        {
            // this should skip all navigation Properties.
            foreach (var property in instance.GetType().GetProperties().Where(p => p.PropertyType.IsValueType || p.PropertyType == typeof(string)))
            {
                object value = this.GetInputValue(property.Name, property.PropertyType, out bool skip);
                if (skip)
                {
                    continue;
                }
                else if (value is null)
                {
                    return false;
                }

                property.SetValue(instance, value);
            }

            return true;
        }

        private void PerformAdd()
        {
            var instance = (T)Activator.CreateInstance(typeof(T));
            if (this.ModifyObject(instance))
            {
                this.logic.Add(instance);
            }
        }

        private object GetInputValue(string propertyName, Type t, out bool skip)
        {
            bool success = false;
            skip = false;
            while (!success)
            {
                this.ClearConsole();
                Console.Write($"Please provide a value for the following property:{Environment.NewLine}{propertyName} ({Nullable.GetUnderlyingType(t)?.Name ?? t.Name}) 'ignore' to skip or 'exit' to cancel. ");
                string input = Console.ReadLine();
                if (input.ToLower() == "exit")
                {
                    return null;
                }
                else if (input.ToLower() == "ignore")
                {
                    skip = true;
                    return null;
                }
                else if (string.IsNullOrEmpty(input))
                {
                    continue;
                }

                try
                {
                    return Convert.ChangeType(input, t, CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                }
            }

            return null;
        }
    }
}