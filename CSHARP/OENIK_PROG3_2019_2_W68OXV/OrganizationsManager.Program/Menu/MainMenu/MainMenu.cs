﻿// <copyright file="MainMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Logic;
    using OrganizationsManager.Logic.JavaInteraction;

    /// <summary>
    /// The main menu of the application.
    /// </summary>
    internal class MainMenu : Menu
    {
        private readonly ILogicFactory logicFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// </summary>
        /// <param name="logicFactory"> the logic factory to use.</param>
        public MainMenu(ILogicFactory logicFactory)
            : base("Main menu actions:", Enum.GetNames(typeof(MainMenuAction))) => this.logicFactory = logicFactory;

        /// <inheritdoc/>
        protected override void PerformAction(int currentAction)
        {
            switch ((MainMenuAction)currentAction)
            {
                case MainMenuAction.OrganizationCRUD:
                    new EntityMenu<Organization>(this.logicFactory.GetOrganizationLogic()).ProcessMenu();
                    break;
                case MainMenuAction.PersonCRUD:
                    new EntityMenu<Person>(this.logicFactory.GetPersonLogic()).ProcessMenu();
                    break;
                case MainMenuAction.VehicleCRUD:
                    new EntityMenu<Vehicle>(this.logicFactory.GetVehicleLogic()).ProcessMenu();
                    break;
                case MainMenuAction.PropertyCRUD:
                    new EntityMenu<Property>(this.logicFactory.GetPropertyLogic()).ProcessMenu();
                    break;
                case MainMenuAction.GroupedTravelDistance:
                    this.OutputResult(this.logicFactory.GetVehicleLogic().GetTotalDistance());
                    break;
                case MainMenuAction.WealthSumPerOrganizationType:
                    this.OutputResult(this.logicFactory.GetOrganizationLogic().GetGroupedWealths());
                    break;
                case MainMenuAction.AverageAgePerOrganization:
                    this.OutputResult(this.logicFactory.GetPersonLogic().GetAveragePersonAge());
                    break;
                case MainMenuAction.JavaInteraction:
                    new JavaMenu(
                        this.logicFactory.GetJavaLogic(),
                        this.logicFactory.GetVehicleLogic(),
                        this.logicFactory.GetPropertyLogic())
                        .ProcessMenu();
                    break;
                case MainMenuAction.Exit:
                    this.CloseMenu();
                    break;
            }
        }
    }
}