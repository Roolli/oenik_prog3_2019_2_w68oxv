﻿// <copyright file="MainMenuAction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines all the available actions for the mainMenu.
    /// </summary>
    internal enum MainMenuAction
    {
        /// <summary>
        /// Organzation entity CRUD actions
        /// </summary>
        OrganizationCRUD,

        /// <summary>
        /// Person entity CRUD actions
        /// </summary>
        PersonCRUD,

        /// <summary>
        /// Vehicle entity CRUD actions
        /// </summary>
        VehicleCRUD,

        /// <summary>
        /// Property entity CRUD actions
        /// </summary>
        PropertyCRUD,

        /// <summary>
        /// GroupedTravelDistance action
        /// </summary>
        GroupedTravelDistance,

        /// <summary>
        /// WealthSumPerOrganizationType
        /// </summary>
        WealthSumPerOrganizationType,

        /// <summary>
        /// AverageAgePerOrganization action
        /// </summary>
        AverageAgePerOrganization,

        /// <summary>
        /// Java web interaction action.
        /// </summary>
        JavaInteraction,

        /// <summary>
        /// Exit action
        /// </summary>
        Exit,
    }
}
