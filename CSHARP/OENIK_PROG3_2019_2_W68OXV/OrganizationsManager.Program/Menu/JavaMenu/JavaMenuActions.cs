﻿// <copyright file="JavaMenuActions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Defines the available actions for the java subMenu.
    /// </summary>
    internal enum JavaMenuActions
    {
        /// <summary>
        /// Vehicle estimation action
        /// </summary>
        EstimateVehicle,

        /// <summary>
        /// Property estimation action
        /// </summary>
        EstimateProperty,

        /// <summary>
        /// Goes back to the main menu.
        /// </summary>
        MainMenu,
    }
}
