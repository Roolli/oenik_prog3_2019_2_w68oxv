﻿// <copyright file="JavaMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Logic;
    using OrganizationsManager.Logic.Buildings;
    using OrganizationsManager.Logic.JavaInteraction;
    using OrganizationsManager.Logic.Vehicles;

    /// <summary>
    /// Handles the Java menu actions.
    /// </summary>
    internal class JavaMenu : Menu
    {
        private IJavaLogic javaLogic;
        private IVehicleLogic vehicleLogic;
        private IPropertyLogic propertyLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaMenu"/> class.
        /// </summary>
        /// <param name="javaLogic">the java logic to use. </param>
        /// <param name="vehicleLogic">the vehicle logic to use. </param>
        /// <param name="propertyLogic"> the property logic to use. </param>
        public JavaMenu(IJavaLogic javaLogic, IVehicleLogic vehicleLogic, IPropertyLogic propertyLogic)
            : base("Select the type of entity to estimate:", Enum.GetNames(typeof(JavaMenuActions)))
        {
            this.javaLogic = javaLogic;
            this.propertyLogic = propertyLogic;
            this.vehicleLogic = vehicleLogic;
        }

        /// <inheritdoc/>
        protected override async void PerformAction(int currentAction)
        {
            switch ((JavaMenuActions)currentAction)
            {
                case JavaMenuActions.EstimateVehicle:
                    await this.ProcessVehicleEstimation();
                    break;
                case JavaMenuActions.EstimateProperty:
                    await this.ProcessPropertyEstimation();
                    break;
                case JavaMenuActions.MainMenu:
                    this.CloseMenu();
                    break;
            }
        }

        private async Task ProcessPropertyEstimation()
        {
            var entities = this.propertyLogic.GetAll().Where(l => l.IsRental == 0).Select(d => new PropertyDTO(d)).ToList();
            var result = await this.javaLogic.GetEstimatedPropertyValue(entities).ConfigureAwait(false);
            this.OutputResult(result);
        }

        private async Task ProcessVehicleEstimation()
        {
            var entities = this.vehicleLogic.GetAll().Where(l => l.IsRental == 0).Select(d => new VehicleDTO(d)).ToList();
            var result = await this.javaLogic.GetEstimatedVehicleValue(entities).ConfigureAwait(false);
            this.OutputResult(result);
        }
    }
}
