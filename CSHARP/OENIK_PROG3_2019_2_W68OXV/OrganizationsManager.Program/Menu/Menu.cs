﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The base class of all menus. This is where the basic loop will reside.
    /// </summary>
    internal abstract class Menu : IMenu
    {
        /// <summary>
        /// The index of the current action.
        /// </summary>
        private int currentAction = 0;

        private string header;
        private string[] actions;
        private bool shouldClose = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="header"> The header of the menu.</param>
        /// <param name="actions">The list of actions that are available.</param>
        protected Menu(string header, string[] actions)
        {
            this.header = header;
            this.actions = actions;
        }

        /// <summary>
        /// Menu render loop.
        /// </summary>
        public void ProcessMenu()
        {
            Console.Clear();
            while (!this.shouldClose)
            {
                this.ListActions();
                this.ProcessMenuMovement(this.GetKeyFromConsole());
            }
        }

        /// <summary>
        /// Outputs the given object instance to the console.
        /// </summary>
        /// <param name="instance"> the object to output.</param>
        protected void OutputResult(object instance)
        {
            Console.Clear();
            string output = instance is IEnumerable<string> collection ? string.Join(Environment.NewLine, collection) : instance.ToString();
            Console.WriteLine(output);
            Console.WriteLine("Press any key to go back!");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// Closes the current menu.
        /// </summary>
        protected void CloseMenu() => this.shouldClose = true;

        /// <summary>
        /// Clears the console much faster.
        /// </summary>
        protected void ClearConsole()
        {
            for (int i = 0; i < this.actions.Length; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write(new string(' ', Console.BufferWidth));
            }

            Console.SetCursorPosition(0, 0);
        }

        /// <summary>
        /// Performs an action based on the current position.
        /// </summary>
        /// <param name="currentAction">the current action's index.</param>
        protected abstract void PerformAction(int currentAction);

        private ConsoleKey GetKeyFromConsole() => Console.ReadKey().Key;

        /// <summary>
        /// Lists all the given options that the menu has.
        /// </summary>
        private void ListActions()
        {
            this.ClearConsole();
            Console.WriteLine(this.header);
            for (int i = 0; i < this.actions.Length; i++)
            {
                if (this.currentAction % this.actions.Length == i)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($">{this.actions[i]}");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.WriteLine($" {this.actions[i]}");
                }
            }
        }

        /// <summary>
        /// Processes the given input from the console.
        /// </summary>
        /// <param name="key"> The key that was pressed on the keyboard.</param>
        private void ProcessMenuMovement(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.Enter:
                    this.PerformAction(this.currentAction);
                    break;
                case ConsoleKey.UpArrow:
                    if (this.currentAction == 0)
                    {
                        this.currentAction = this.actions.Length - 1;
                    }
                    else
                    {
                        this.currentAction--;
                    }

                    break;
                case ConsoleKey.DownArrow:
                    if (this.currentAction == this.actions.Length - 1)
                    {
                        this.currentAction = 0;
                    }
                    else
                    {
                        this.currentAction++;
                    }

                    break;
            }
        }
    }
}
