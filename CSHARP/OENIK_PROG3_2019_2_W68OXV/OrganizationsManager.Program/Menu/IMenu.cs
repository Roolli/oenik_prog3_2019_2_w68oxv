﻿// <copyright file="IMenu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program.Menu
{
    /// <summary>
    /// Defines the behaviour for the Menu class.
    /// </summary>
    public interface IMenu
    {
        /// <summary>
        /// Menu Loop.
        /// </summary>
        void ProcessMenu();
    }
}
