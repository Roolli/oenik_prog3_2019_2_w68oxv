﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Program
{
    using OrganizationsManager.Data;
    using OrganizationsManager.Logic;
    using OrganizationsManager.Program.Menu;

    /// <summary>
    /// Program class.
    /// </summary>
    public class Program
    {
        private static readonly ILogicFactory LogicFactory = new LogicFactory();

        /// <summary>
        ///  Startup method.
        /// </summary>
        internal static void Main() => new MainMenu(LogicFactory).ProcessMenu();
    }
}
