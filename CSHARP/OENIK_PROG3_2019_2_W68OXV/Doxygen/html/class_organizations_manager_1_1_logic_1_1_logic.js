var class_organizations_manager_1_1_logic_1_1_logic =
[
    [ "Logic", "class_organizations_manager_1_1_logic_1_1_logic.html#a0374078f5612bf8345ad9e0f900a1bca", null ],
    [ "Add", "class_organizations_manager_1_1_logic_1_1_logic.html#ae05943d9b0e26a1912c9c54a9b74c55e", null ],
    [ "Delete", "class_organizations_manager_1_1_logic_1_1_logic.html#acbfc1b717645582f48864dd86e7754bb", null ],
    [ "Get", "class_organizations_manager_1_1_logic_1_1_logic.html#a7ff9293edf505a48fa4bb02685a53385", null ],
    [ "GetAll", "class_organizations_manager_1_1_logic_1_1_logic.html#ac23e49e7631c05a1ce7901ffcd89df7d", null ],
    [ "Update", "class_organizations_manager_1_1_logic_1_1_logic.html#a7c995825f9fef0d1ec389caa0d2c218d", null ],
    [ "Repository", "class_organizations_manager_1_1_logic_1_1_logic.html#a092a9617a3185aa9d50038b6dd7d7021", null ]
];