var class_organizations_manager_1_1_data_1_1_vehicle =
[
    [ "Equals", "class_organizations_manager_1_1_data_1_1_vehicle.html#abb646767fa0e8486a7b9c32e947da129", null ],
    [ "GetHashCode", "class_organizations_manager_1_1_data_1_1_vehicle.html#a78d7a9ddb0176b319e7c56d08372d121", null ],
    [ "ToString", "class_organizations_manager_1_1_data_1_1_vehicle.html#a26fb4037536ad970b4c4848b46f38529", null ],
    [ "AquireDate", "class_organizations_manager_1_1_data_1_1_vehicle.html#a9ecb43808a2acab78694e4c1dae1c612", null ],
    [ "Color", "class_organizations_manager_1_1_data_1_1_vehicle.html#a54b8bf506a83a5e88f819a82ed11695f", null ],
    [ "DistanceTravelled", "class_organizations_manager_1_1_data_1_1_vehicle.html#ab8749629600420138320a5b96b6e1d5d", null ],
    [ "IsRental", "class_organizations_manager_1_1_data_1_1_vehicle.html#a7a450f8563e6515c6f2e6f2c06b9ede3", null ],
    [ "LicensePlate", "class_organizations_manager_1_1_data_1_1_vehicle.html#a12fa1b6ca23a0022f54a77e7257f7bfa", null ],
    [ "Make", "class_organizations_manager_1_1_data_1_1_vehicle.html#af161ea5d24aa782b637d78a79d094f3d", null ],
    [ "Model", "class_organizations_manager_1_1_data_1_1_vehicle.html#a6303a2e8d65a568e85b6920b971cbd3b", null ],
    [ "Organization", "class_organizations_manager_1_1_data_1_1_vehicle.html#ab31cb14b1e13e56c0d505244a79b9508", null ],
    [ "OwnerId", "class_organizations_manager_1_1_data_1_1_vehicle.html#a352e2fe0a8dd3875f98944210e4d23fd", null ]
];