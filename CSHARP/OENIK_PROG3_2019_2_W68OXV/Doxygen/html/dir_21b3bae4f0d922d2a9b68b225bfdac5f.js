var dir_21b3bae4f0d922d2a9b68b225bfdac5f =
[
    [ "Buildings", "dir_87e280a39d7b0baa0dff0c500a1993f5.html", "dir_87e280a39d7b0baa0dff0c500a1993f5" ],
    [ "JavaInteraction", "dir_8aed368e21c5c8b67fda7e1b96f2a0a5.html", "dir_8aed368e21c5c8b67fda7e1b96f2a0a5" ],
    [ "Organizations", "dir_26b58e0a163e0ceb1bc3feb6a2e2153c.html", "dir_26b58e0a163e0ceb1bc3feb6a2e2153c" ],
    [ "People", "dir_97b2f8afdc80d333612cfe927b696b43.html", "dir_97b2f8afdc80d333612cfe927b696b43" ],
    [ "Properties", "dir_bd6d6be8b65a003733cb84375bb1737b.html", "dir_bd6d6be8b65a003733cb84375bb1737b" ],
    [ "Vehicles", "dir_f64d380b2d3e178cdee593c3a9a59fce.html", "dir_f64d380b2d3e178cdee593c3a9a59fce" ],
    [ "ILogic.cs", "_i_logic_8cs_source.html", null ],
    [ "ILogicFactory.cs", "_i_logic_factory_8cs_source.html", null ],
    [ "Logic.cs", "_logic_8cs_source.html", null ],
    [ "LogicFactory.cs", "_logic_factory_8cs_source.html", null ]
];