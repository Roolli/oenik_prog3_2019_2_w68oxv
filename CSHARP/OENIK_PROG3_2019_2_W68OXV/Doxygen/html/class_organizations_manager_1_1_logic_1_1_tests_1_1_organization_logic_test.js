var class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test =
[
    [ "GetOne", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#a06c3a5626d0838cbec8062caf43085a6", null ],
    [ "Setup", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#a4b09b8c69be78fcc2542aa0d97155fcb", null ],
    [ "TestAdd", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#a93c762ed72f587e370985a1d705c1731", null ],
    [ "TestGetAll", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#ac1261362d1b10ca316dba5b6366ee43c", null ],
    [ "TestGetGroupedWealths", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#a92d96f5cd9e0e3d11c58db45f64ee367", null ],
    [ "TestUpdate", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html#afa04419d83e47b2bf939b14e36b9b2e7", null ]
];