var class_organizations_manager_1_1_data_1_1_person =
[
    [ "Equals", "class_organizations_manager_1_1_data_1_1_person.html#ae214c9d67dc2eb28c30d07400444fe29", null ],
    [ "GetHashCode", "class_organizations_manager_1_1_data_1_1_person.html#acf16671f8a1394894088af43d2b13fe3", null ],
    [ "ToString", "class_organizations_manager_1_1_data_1_1_person.html#a6c5366cb0db3fe32d28a63e4ac13883d", null ],
    [ "City", "class_organizations_manager_1_1_data_1_1_person.html#a6d64a1085ec9bb5a2589952b5c21f83f", null ],
    [ "DateOfBirth", "class_organizations_manager_1_1_data_1_1_person.html#a1b387e646a9647b5fc94ddc3983e993e", null ],
    [ "Email", "class_organizations_manager_1_1_data_1_1_person.html#ad8f109fdedb30bdaa7e0a5d732a59ab2", null ],
    [ "Gender", "class_organizations_manager_1_1_data_1_1_person.html#ae1a8683fd1a824b6470f8261acd6445b", null ],
    [ "Id", "class_organizations_manager_1_1_data_1_1_person.html#a9f7cf991089949df4138ea3ce27dd35a", null ],
    [ "Organization", "class_organizations_manager_1_1_data_1_1_person.html#a48df0ccefb1598cf4c66cfd3c7ebca88", null ],
    [ "OrganizationID", "class_organizations_manager_1_1_data_1_1_person.html#a317a345e043c33548ed68fd8285e9cbc", null ],
    [ "PersonName", "class_organizations_manager_1_1_data_1_1_person.html#a497c5afce54c5758857e2f3d4065bc92", null ]
];