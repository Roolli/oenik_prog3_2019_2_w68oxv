var searchData=
[
  ['licenseplate_62',['LicensePlate',['../class_organizations_manager_1_1_data_1_1_vehicle.html#a12fa1b6ca23a0022f54a77e7257f7bfa',1,'OrganizationsManager::Data::Vehicle']]],
  ['logic_63',['Logic',['../class_organizations_manager_1_1_logic_1_1_logic.html',1,'OrganizationsManager.Logic.Logic&lt; T &gt;'],['../class_organizations_manager_1_1_logic_1_1_logic.html#a0374078f5612bf8345ad9e0f900a1bca',1,'OrganizationsManager.Logic.Logic.Logic()']]],
  ['logic_3c_20organization_20_3e_64',['Logic&lt; Organization &gt;',['../class_organizations_manager_1_1_logic_1_1_logic.html',1,'OrganizationsManager::Logic']]],
  ['logic_3c_20person_20_3e_65',['Logic&lt; Person &gt;',['../class_organizations_manager_1_1_logic_1_1_logic.html',1,'OrganizationsManager::Logic']]],
  ['logic_3c_20property_20_3e_66',['Logic&lt; Property &gt;',['../class_organizations_manager_1_1_logic_1_1_logic.html',1,'OrganizationsManager::Logic']]],
  ['logic_3c_20vehicle_20_3e_67',['Logic&lt; Vehicle &gt;',['../class_organizations_manager_1_1_logic_1_1_logic.html',1,'OrganizationsManager::Logic']]],
  ['logicfactory_68',['LogicFactory',['../class_organizations_manager_1_1_logic_1_1_logic_factory.html',1,'OrganizationsManager::Logic']]],
  ['license_69',['LICENSE',['../md__c_1__users__rooll_source_repos_oenik_prog3_2019_2_w68oxv__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2f976e5afccc677ff022fd4a8cc4e18c0.html',1,'']]]
];
