var searchData=
[
  ['person_98',['Person',['../class_organizations_manager_1_1_data_1_1_person.html',1,'OrganizationsManager.Data.Person'],['../class_organizations_manager_1_1_data_1_1_organization.html#a14ed3e3e519d3ecfc599340940325e43',1,'OrganizationsManager.Data.Organization.Person()']]],
  ['personname_99',['PersonName',['../class_organizations_manager_1_1_data_1_1_person.html#a497c5afce54c5758857e2f3d4065bc92',1,'OrganizationsManager::Data::Person']]],
  ['processmenu_100',['ProcessMenu',['../interface_organizations_manager_1_1_program_1_1_menu_1_1_i_menu.html#a67f25ea453e716ba4a9b975d5a4db38a',1,'OrganizationsManager::Program::Menu::IMenu']]],
  ['program_101',['Program',['../class_organizations_manager_1_1_program_1_1_program.html',1,'OrganizationsManager::Program']]],
  ['property_102',['Property',['../class_organizations_manager_1_1_data_1_1_property.html',1,'OrganizationsManager.Data.Property'],['../class_organizations_manager_1_1_data_1_1_organization.html#a5765c5e60491668f403c8a04fc08bd41',1,'OrganizationsManager.Data.Organization.Property()']]],
  ['propertyaddress_103',['PropertyAddress',['../class_organizations_manager_1_1_data_1_1_property.html#a9b499192ebe7f60ae1f06f9d8ec57ee9',1,'OrganizationsManager::Data::Property']]],
  ['propertydto_104',['PropertyDTO',['../class_organizations_manager_1_1_logic_1_1_java_interaction_1_1_property_d_t_o.html',1,'OrganizationsManager::Logic::JavaInteraction']]]
];
