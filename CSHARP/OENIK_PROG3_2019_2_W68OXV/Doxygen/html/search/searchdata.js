var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstuvw",
  1: "agiloprv",
  2: "o",
  3: "adeglopstu",
  4: "abcdefgilmnoprtvw",
  5: "aclmn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "properties",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Properties",
  5: "Pages"
};

