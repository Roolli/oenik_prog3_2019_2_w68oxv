var searchData=
[
  ['add_0',['Add',['../interface_organizations_manager_1_1_logic_1_1_i_logic.html#a26e78ba7f319a66f1c9199479b1ac40d',1,'OrganizationsManager.Logic.ILogic.Add()'],['../class_organizations_manager_1_1_logic_1_1_logic.html#ae05943d9b0e26a1912c9c54a9b74c55e',1,'OrganizationsManager.Logic.Logic.Add()'],['../interface_organizations_manager_1_1_repository_1_1_i_repository.html#a56724b0f64a12a12cd5e3aa10a4e56f7',1,'OrganizationsManager.Repository.IRepository.Add()'],['../class_organizations_manager_1_1_repository_1_1_repository.html#a1210671231ea721d48e9a483056dc76a',1,'OrganizationsManager.Repository.Repository.Add()']]],
  ['aquiredate_1',['AquireDate',['../class_organizations_manager_1_1_data_1_1_property.html#aab2686fc9f6a18daf8af74a5127f279c',1,'OrganizationsManager.Data.Property.AquireDate()'],['../class_organizations_manager_1_1_data_1_1_vehicle.html#a9ecb43808a2acab78694e4c1dae1c612',1,'OrganizationsManager.Data.Vehicle.AquireDate()']]],
  ['averageage_2',['AverageAge',['../class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#accf78c934f4be7cd7f683ec1b0716f95',1,'OrganizationsManager::Repository::DTOs::AveragePersonAgeDTO']]],
  ['averagepersonagedto_3',['AveragePersonAgeDTO',['../class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html',1,'OrganizationsManager::Repository::DTOs']]],
  ['analyzer_20configuration_4',['Analyzer Configuration',['../md__c_1__users__rooll_source_repos_oenik_prog3_2019_2_w68oxv__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_237b600e9b71b496899c622ef111784e6.html',1,'']]]
];
