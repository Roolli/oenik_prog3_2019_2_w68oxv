var searchData=
[
  ['buildings_76',['Buildings',['../namespace_organizations_manager_1_1_logic_1_1_buildings.html',1,'OrganizationsManager.Logic.Buildings'],['../namespace_organizations_manager_1_1_repository_1_1_buildings.html',1,'OrganizationsManager.Repository.Buildings']]],
  ['data_77',['Data',['../namespace_organizations_manager_1_1_data.html',1,'OrganizationsManager']]],
  ['dtos_78',['DTOS',['../namespace_organizations_manager_1_1_repository_1_1_d_t_o_s.html',1,'OrganizationsManager.Repository.DTOS'],['../namespace_organizations_manager_1_1_repository_1_1_d_t_os.html',1,'OrganizationsManager.Repository.DTOs']]],
  ['javainteraction_79',['JavaInteraction',['../namespace_organizations_manager_1_1_logic_1_1_java_interaction.html',1,'OrganizationsManager::Logic']]],
  ['logic_80',['Logic',['../namespace_organizations_manager_1_1_logic.html',1,'OrganizationsManager']]],
  ['menu_81',['Menu',['../namespace_organizations_manager_1_1_program_1_1_menu.html',1,'OrganizationsManager::Program']]],
  ['operator_20propertydto_82',['operator PropertyDTO',['../class_organizations_manager_1_1_logic_1_1_java_interaction_1_1_property_d_t_o.html#ac23dbab24cecb1efb09a69f1d3d0194e',1,'OrganizationsManager::Logic::JavaInteraction::PropertyDTO']]],
  ['operator_20vehicledto_83',['operator VehicleDTO',['../class_organizations_manager_1_1_logic_1_1_java_interaction_1_1_vehicle_d_t_o.html#a6b6560eee328cb859076d2770b5aecb1',1,'OrganizationsManager::Logic::JavaInteraction::VehicleDTO']]],
  ['organization_84',['Organization',['../class_organizations_manager_1_1_data_1_1_organization.html',1,'OrganizationsManager.Data.Organization'],['../class_organizations_manager_1_1_data_1_1_person.html#a48df0ccefb1598cf4c66cfd3c7ebca88',1,'OrganizationsManager.Data.Person.Organization()'],['../class_organizations_manager_1_1_data_1_1_property.html#ac44e9da7e9c0ce5f1807a1fbdcc58d93',1,'OrganizationsManager.Data.Property.Organization()'],['../class_organizations_manager_1_1_data_1_1_vehicle.html#ab31cb14b1e13e56c0d505244a79b9508',1,'OrganizationsManager.Data.Vehicle.Organization()'],['../class_organizations_manager_1_1_data_1_1_organization.html#abcf9e15adf52c4e6379c2808340edbe2',1,'OrganizationsManager.Data.Organization.Organization()']]],
  ['organizationid_85',['OrganizationID',['../class_organizations_manager_1_1_data_1_1_person.html#a317a345e043c33548ed68fd8285e9cbc',1,'OrganizationsManager.Data.Person.OrganizationID()'],['../class_organizations_manager_1_1_repository_1_1_d_t_o_s_1_1_grouped_vehicle_travel_distance_d_t_o.html#ad4cccecdfff2d91290a0fb17e5c31271',1,'OrganizationsManager.Repository.DTOS.GroupedVehicleTravelDistanceDTO.OrganizationId()']]],
  ['organizationlogictest_86',['OrganizationLogicTest',['../class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html',1,'OrganizationsManager::Logic::Tests']]],
  ['organizationname_87',['OrganizationName',['../class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#aa5c1f3792906036b0ab6b1a0b6812f66',1,'OrganizationsManager::Repository::DTOs::AveragePersonAgeDTO']]],
  ['organizations_88',['Organizations',['../namespace_organizations_manager_1_1_repository_1_1_organizations.html',1,'OrganizationsManager::Repository']]],
  ['organizationscontext_89',['OrganizationsContext',['../class_organizations_manager_1_1_data_1_1_organizations_context.html',1,'OrganizationsManager::Data']]],
  ['organizationsmanager_90',['OrganizationsManager',['../namespace_organizations_manager.html',1,'']]],
  ['organizationtype_91',['OrganizationType',['../class_organizations_manager_1_1_repository_1_1_d_t_o_s_1_1_grouped_wealth_d_t_o.html#ad7ca3209beb4734f2284e5a8365e6fde',1,'OrganizationsManager::Repository::DTOS::GroupedWealthDTO']]],
  ['ownerid_92',['OwnerID',['../class_organizations_manager_1_1_data_1_1_property.html#a49c20d84529b9242d2c2619842bfc276',1,'OrganizationsManager.Data.Property.OwnerID()'],['../class_organizations_manager_1_1_data_1_1_vehicle.html#a352e2fe0a8dd3875f98944210e4d23fd',1,'OrganizationsManager.Data.Vehicle.OwnerId()']]],
  ['people_93',['People',['../namespace_organizations_manager_1_1_repository_1_1_people.html',1,'OrganizationsManager::Repository']]],
  ['program_94',['Program',['../namespace_organizations_manager_1_1_program.html',1,'OrganizationsManager']]],
  ['repository_95',['Repository',['../namespace_organizations_manager_1_1_repository.html',1,'OrganizationsManager']]],
  ['tests_96',['Tests',['../namespace_organizations_manager_1_1_logic_1_1_tests.html',1,'OrganizationsManager::Logic']]],
  ['vehicles_97',['Vehicles',['../namespace_organizations_manager_1_1_logic_1_1_vehicles.html',1,'OrganizationsManager.Logic.Vehicles'],['../namespace_organizations_manager_1_1_repository_1_1_vehicles.html',1,'OrganizationsManager.Repository.Vehicles']]]
];
