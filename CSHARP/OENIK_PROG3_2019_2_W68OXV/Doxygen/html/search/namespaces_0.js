var searchData=
[
  ['buildings_168',['Buildings',['../namespace_organizations_manager_1_1_logic_1_1_buildings.html',1,'OrganizationsManager.Logic.Buildings'],['../namespace_organizations_manager_1_1_repository_1_1_buildings.html',1,'OrganizationsManager.Repository.Buildings']]],
  ['data_169',['Data',['../namespace_organizations_manager_1_1_data.html',1,'OrganizationsManager']]],
  ['dtos_170',['DTOS',['../namespace_organizations_manager_1_1_repository_1_1_d_t_o_s.html',1,'OrganizationsManager.Repository.DTOS'],['../namespace_organizations_manager_1_1_repository_1_1_d_t_os.html',1,'OrganizationsManager.Repository.DTOs']]],
  ['javainteraction_171',['JavaInteraction',['../namespace_organizations_manager_1_1_logic_1_1_java_interaction.html',1,'OrganizationsManager::Logic']]],
  ['logic_172',['Logic',['../namespace_organizations_manager_1_1_logic.html',1,'OrganizationsManager']]],
  ['menu_173',['Menu',['../namespace_organizations_manager_1_1_program_1_1_menu.html',1,'OrganizationsManager::Program']]],
  ['organizations_174',['Organizations',['../namespace_organizations_manager_1_1_repository_1_1_organizations.html',1,'OrganizationsManager::Repository']]],
  ['organizationsmanager_175',['OrganizationsManager',['../namespace_organizations_manager.html',1,'']]],
  ['people_176',['People',['../namespace_organizations_manager_1_1_repository_1_1_people.html',1,'OrganizationsManager::Repository']]],
  ['program_177',['Program',['../namespace_organizations_manager_1_1_program.html',1,'OrganizationsManager']]],
  ['repository_178',['Repository',['../namespace_organizations_manager_1_1_repository.html',1,'OrganizationsManager']]],
  ['tests_179',['Tests',['../namespace_organizations_manager_1_1_logic_1_1_tests.html',1,'OrganizationsManager::Logic']]],
  ['vehicles_180',['Vehicles',['../namespace_organizations_manager_1_1_logic_1_1_vehicles.html',1,'OrganizationsManager.Logic.Vehicles'],['../namespace_organizations_manager_1_1_repository_1_1_vehicles.html',1,'OrganizationsManager.Repository.Vehicles']]]
];
