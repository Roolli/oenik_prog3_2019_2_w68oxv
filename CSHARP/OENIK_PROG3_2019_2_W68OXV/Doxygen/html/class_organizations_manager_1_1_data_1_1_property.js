var class_organizations_manager_1_1_data_1_1_property =
[
    [ "Equals", "class_organizations_manager_1_1_data_1_1_property.html#a1fd22adae360ce9d9b268924255d3314", null ],
    [ "GetHashCode", "class_organizations_manager_1_1_data_1_1_property.html#accd5f86eb83b1d7d2727d0ab3c4f907c", null ],
    [ "ToString", "class_organizations_manager_1_1_data_1_1_property.html#af7f464dfced384712db742f6b52dc5bb", null ],
    [ "AquireDate", "class_organizations_manager_1_1_data_1_1_property.html#aab2686fc9f6a18daf8af74a5127f279c", null ],
    [ "BuildingSize", "class_organizations_manager_1_1_data_1_1_property.html#ad158aeea21650af8266a817e3af9e750", null ],
    [ "City", "class_organizations_manager_1_1_data_1_1_property.html#af4acf3c43aba31797d875463b841ac1a", null ],
    [ "Id", "class_organizations_manager_1_1_data_1_1_property.html#a75deffbcffbe8008d1530d1b93d03663", null ],
    [ "IsRental", "class_organizations_manager_1_1_data_1_1_property.html#ac3499c3da4b51a9c0340b835bb34d2a2", null ],
    [ "Organization", "class_organizations_manager_1_1_data_1_1_property.html#ac44e9da7e9c0ce5f1807a1fbdcc58d93", null ],
    [ "OwnerID", "class_organizations_manager_1_1_data_1_1_property.html#a49c20d84529b9242d2c2619842bfc276", null ],
    [ "PropertyAddress", "class_organizations_manager_1_1_data_1_1_property.html#a9b499192ebe7f60ae1f06f9d8ec57ee9", null ]
];