var hierarchy =
[
    [ "OrganizationsManager.Repository.DTOs.AveragePersonAgeDTO", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html", null ],
    [ "DbContext", null, [
      [ "OrganizationsManager.Data::OrganizationsContext", "class_organizations_manager_1_1_data_1_1_organizations_context.html", null ]
    ] ],
    [ "OrganizationsManager.Repository.DTOS.GroupedVehicleTravelDistanceDTO", "class_organizations_manager_1_1_repository_1_1_d_t_o_s_1_1_grouped_vehicle_travel_distance_d_t_o.html", null ],
    [ "OrganizationsManager.Repository.DTOS.GroupedWealthDTO", "class_organizations_manager_1_1_repository_1_1_d_t_o_s_1_1_grouped_wealth_d_t_o.html", null ],
    [ "OrganizationsManager.Logic.JavaInteraction.IJavaLogic", "interface_organizations_manager_1_1_logic_1_1_java_interaction_1_1_i_java_logic.html", null ],
    [ "OrganizationsManager.Logic.ILogic< T >", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", [
      [ "OrganizationsManager.Logic.Logic< T >", "class_organizations_manager_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.ILogic< Organization >", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", [
      [ "OrganizationsManager.Logic.IOrganizationLogic", "interface_organizations_manager_1_1_logic_1_1_i_organization_logic.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.ILogic< Person >", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", [
      [ "OrganizationsManager.Logic.IPersonLogic", "interface_organizations_manager_1_1_logic_1_1_i_person_logic.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.ILogic< Property >", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", [
      [ "OrganizationsManager.Logic.Buildings.IPropertyLogic", "interface_organizations_manager_1_1_logic_1_1_buildings_1_1_i_property_logic.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.ILogic< Vehicle >", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", [
      [ "OrganizationsManager.Logic.Vehicles.IVehicleLogic", "interface_organizations_manager_1_1_logic_1_1_vehicles_1_1_i_vehicle_logic.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.ILogicFactory", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html", [
      [ "OrganizationsManager.Logic.LogicFactory", "class_organizations_manager_1_1_logic_1_1_logic_factory.html", null ]
    ] ],
    [ "OrganizationsManager.Program.Menu.IMenu", "interface_organizations_manager_1_1_program_1_1_menu_1_1_i_menu.html", null ],
    [ "OrganizationsManager.Repository.IRepository< T >", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", [
      [ "OrganizationsManager.Repository.Repository< T >", "class_organizations_manager_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "OrganizationsManager.Repository.IRepository< Organization >", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", [
      [ "OrganizationsManager.Repository.Organizations.IOrganizationRepository", "interface_organizations_manager_1_1_repository_1_1_organizations_1_1_i_organization_repository.html", null ]
    ] ],
    [ "OrganizationsManager.Repository.IRepository< Person >", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", [
      [ "OrganizationsManager.Repository.People.IPersonRepository", "interface_organizations_manager_1_1_repository_1_1_people_1_1_i_person_repository.html", null ]
    ] ],
    [ "OrganizationsManager.Repository.IRepository< Property >", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", [
      [ "OrganizationsManager.Repository.Buildings.IPropertyRepository", "interface_organizations_manager_1_1_repository_1_1_buildings_1_1_i_property_repository.html", null ]
    ] ],
    [ "OrganizationsManager.Repository.IRepository< Vehicle >", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", [
      [ "OrganizationsManager.Repository.Vehicles.IVehicleRepository", "interface_organizations_manager_1_1_repository_1_1_vehicles_1_1_i_vehicle_repository.html", null ]
    ] ],
    [ "OrganizationsManager.Logic.Logic< Organization >", "class_organizations_manager_1_1_logic_1_1_logic.html", null ],
    [ "OrganizationsManager.Logic.Logic< Person >", "class_organizations_manager_1_1_logic_1_1_logic.html", null ],
    [ "OrganizationsManager.Logic.Logic< Property >", "class_organizations_manager_1_1_logic_1_1_logic.html", null ],
    [ "OrganizationsManager.Logic.Logic< Vehicle >", "class_organizations_manager_1_1_logic_1_1_logic.html", null ],
    [ "OrganizationsManager.Data.Organization", "class_organizations_manager_1_1_data_1_1_organization.html", null ],
    [ "OrganizationsManager.Logic.Tests.OrganizationLogicTest", "class_organizations_manager_1_1_logic_1_1_tests_1_1_organization_logic_test.html", null ],
    [ "OrganizationsManager.Data.Person", "class_organizations_manager_1_1_data_1_1_person.html", null ],
    [ "OrganizationsManager.Program.Program", "class_organizations_manager_1_1_program_1_1_program.html", null ],
    [ "OrganizationsManager.Data.Property", "class_organizations_manager_1_1_data_1_1_property.html", null ],
    [ "OrganizationsManager.Logic.JavaInteraction.PropertyDTO", "class_organizations_manager_1_1_logic_1_1_java_interaction_1_1_property_d_t_o.html", null ],
    [ "OrganizationsManager.Repository.Repository< Organization >", "class_organizations_manager_1_1_repository_1_1_repository.html", null ],
    [ "OrganizationsManager.Repository.Repository< Person >", "class_organizations_manager_1_1_repository_1_1_repository.html", null ],
    [ "OrganizationsManager.Repository.Repository< Property >", "class_organizations_manager_1_1_repository_1_1_repository.html", null ],
    [ "OrganizationsManager.Repository.Repository< Vehicle >", "class_organizations_manager_1_1_repository_1_1_repository.html", null ],
    [ "OrganizationsManager.Repository.RepositoryFactory", "class_organizations_manager_1_1_repository_1_1_repository_factory.html", null ],
    [ "OrganizationsManager.Data.Vehicle", "class_organizations_manager_1_1_data_1_1_vehicle.html", null ],
    [ "OrganizationsManager.Logic.JavaInteraction.VehicleDTO", "class_organizations_manager_1_1_logic_1_1_java_interaction_1_1_vehicle_d_t_o.html", null ]
];