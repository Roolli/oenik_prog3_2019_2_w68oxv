var class_organizations_manager_1_1_logic_1_1_logic_factory =
[
    [ "GetJavaLogic", "class_organizations_manager_1_1_logic_1_1_logic_factory.html#ac5e2992512e1bac6e2ab534f02512ab9", null ],
    [ "GetOrganizationLogic", "class_organizations_manager_1_1_logic_1_1_logic_factory.html#a6192d586a7295dbf38d70d33bcc20cb0", null ],
    [ "GetPersonLogic", "class_organizations_manager_1_1_logic_1_1_logic_factory.html#a5cb90aa153f388a2c072102b2dba9bd4", null ],
    [ "GetPropertyLogic", "class_organizations_manager_1_1_logic_1_1_logic_factory.html#a03431062435ee8ab549665cb86f99b7d", null ],
    [ "GetVehicleLogic", "class_organizations_manager_1_1_logic_1_1_logic_factory.html#af26ffaf0607c48f9cd8855658d3132c1", null ]
];