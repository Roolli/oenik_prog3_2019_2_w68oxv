var namespace_organizations_manager_1_1_logic =
[
    [ "Buildings", "namespace_organizations_manager_1_1_logic_1_1_buildings.html", "namespace_organizations_manager_1_1_logic_1_1_buildings" ],
    [ "JavaInteraction", "namespace_organizations_manager_1_1_logic_1_1_java_interaction.html", "namespace_organizations_manager_1_1_logic_1_1_java_interaction" ],
    [ "Tests", "namespace_organizations_manager_1_1_logic_1_1_tests.html", "namespace_organizations_manager_1_1_logic_1_1_tests" ],
    [ "Vehicles", "namespace_organizations_manager_1_1_logic_1_1_vehicles.html", "namespace_organizations_manager_1_1_logic_1_1_vehicles" ],
    [ "ILogic", "interface_organizations_manager_1_1_logic_1_1_i_logic.html", "interface_organizations_manager_1_1_logic_1_1_i_logic" ],
    [ "ILogicFactory", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory" ],
    [ "IOrganizationLogic", "interface_organizations_manager_1_1_logic_1_1_i_organization_logic.html", "interface_organizations_manager_1_1_logic_1_1_i_organization_logic" ],
    [ "IPersonLogic", "interface_organizations_manager_1_1_logic_1_1_i_person_logic.html", "interface_organizations_manager_1_1_logic_1_1_i_person_logic" ],
    [ "Logic", "class_organizations_manager_1_1_logic_1_1_logic.html", "class_organizations_manager_1_1_logic_1_1_logic" ],
    [ "LogicFactory", "class_organizations_manager_1_1_logic_1_1_logic_factory.html", "class_organizations_manager_1_1_logic_1_1_logic_factory" ]
];