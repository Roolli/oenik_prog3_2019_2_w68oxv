var class_organizations_manager_1_1_data_1_1_organization =
[
    [ "Organization", "class_organizations_manager_1_1_data_1_1_organization.html#abcf9e15adf52c4e6379c2808340edbe2", null ],
    [ "Equals", "class_organizations_manager_1_1_data_1_1_organization.html#a5b267a32f02e2bc8afca83ff27ead866", null ],
    [ "GetHashCode", "class_organizations_manager_1_1_data_1_1_organization.html#a07a15ecaec7f96417e8cd9d4616fe93e", null ],
    [ "ToString", "class_organizations_manager_1_1_data_1_1_organization.html#a73530318564b897fcda43aa5ad7c032b", null ],
    [ "FoundationDate", "class_organizations_manager_1_1_data_1_1_organization.html#af3094cda657d2a203e7447d950a57973", null ],
    [ "GroupType", "class_organizations_manager_1_1_data_1_1_organization.html#a320808ec72ee1cf2f95115ed351c8fb2", null ],
    [ "Id", "class_organizations_manager_1_1_data_1_1_organization.html#ab624a6bbabc392362388596d37085174", null ],
    [ "MemberCount", "class_organizations_manager_1_1_data_1_1_organization.html#a14329ce9642b5c0eaf347f5e68b40bac", null ],
    [ "Name", "class_organizations_manager_1_1_data_1_1_organization.html#a7f54384a4acad1b1b601f8836ed7cf7d", null ],
    [ "Person", "class_organizations_manager_1_1_data_1_1_organization.html#a14ed3e3e519d3ecfc599340940325e43", null ],
    [ "Property", "class_organizations_manager_1_1_data_1_1_organization.html#a5765c5e60491668f403c8a04fc08bd41", null ],
    [ "Vault", "class_organizations_manager_1_1_data_1_1_organization.html#a9e42e5898571297878dd9ba644e9ef52", null ],
    [ "Vehicle", "class_organizations_manager_1_1_data_1_1_organization.html#a203b34b299084e3a4d81067a9161da67", null ]
];