var interface_organizations_manager_1_1_logic_1_1_i_logic_factory =
[
    [ "GetJavaLogic", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html#a8b336f5e563d7cb9eda6aeecce42c28a", null ],
    [ "GetOrganizationLogic", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html#ad81ddc664b1d10cae686a932f8ee9826", null ],
    [ "GetPersonLogic", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html#aff7eade2af105b725b20f53b1ff47a1a", null ],
    [ "GetPropertyLogic", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html#addde7acd8842b7ba154a869a490cc2e7", null ],
    [ "GetVehicleLogic", "interface_organizations_manager_1_1_logic_1_1_i_logic_factory.html#a8e50ddb128d4b629e32d1b01f3843774", null ]
];