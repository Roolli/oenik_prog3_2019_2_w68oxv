var namespace_organizations_manager_1_1_data =
[
    [ "Organization", "class_organizations_manager_1_1_data_1_1_organization.html", "class_organizations_manager_1_1_data_1_1_organization" ],
    [ "OrganizationsContext", "class_organizations_manager_1_1_data_1_1_organizations_context.html", null ],
    [ "Person", "class_organizations_manager_1_1_data_1_1_person.html", "class_organizations_manager_1_1_data_1_1_person" ],
    [ "Property", "class_organizations_manager_1_1_data_1_1_property.html", "class_organizations_manager_1_1_data_1_1_property" ],
    [ "Vehicle", "class_organizations_manager_1_1_data_1_1_vehicle.html", "class_organizations_manager_1_1_data_1_1_vehicle" ]
];