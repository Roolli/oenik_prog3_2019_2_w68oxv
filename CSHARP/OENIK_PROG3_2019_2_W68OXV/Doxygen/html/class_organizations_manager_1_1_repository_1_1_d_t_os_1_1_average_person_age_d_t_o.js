var class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o =
[
    [ "Equals", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#a06c1a2f71c0c17d99791cc0f038051d1", null ],
    [ "GetHashCode", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#addbf8f46867c37cc50669443ceaa61e6", null ],
    [ "ToString", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#ae97ded394b08d6d4dd051c5040b2f5e5", null ],
    [ "AverageAge", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#accf78c934f4be7cd7f683ec1b0716f95", null ],
    [ "OrganizationName", "class_organizations_manager_1_1_repository_1_1_d_t_os_1_1_average_person_age_d_t_o.html#aa5c1f3792906036b0ab6b1a0b6812f66", null ]
];