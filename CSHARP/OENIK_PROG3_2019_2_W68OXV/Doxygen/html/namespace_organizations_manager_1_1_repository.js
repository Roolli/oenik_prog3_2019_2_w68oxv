var namespace_organizations_manager_1_1_repository =
[
    [ "Buildings", "namespace_organizations_manager_1_1_repository_1_1_buildings.html", "namespace_organizations_manager_1_1_repository_1_1_buildings" ],
    [ "DTOS", "namespace_organizations_manager_1_1_repository_1_1_d_t_o_s.html", "namespace_organizations_manager_1_1_repository_1_1_d_t_o_s" ],
    [ "DTOs", "namespace_organizations_manager_1_1_repository_1_1_d_t_os.html", "namespace_organizations_manager_1_1_repository_1_1_d_t_os" ],
    [ "Organizations", "namespace_organizations_manager_1_1_repository_1_1_organizations.html", "namespace_organizations_manager_1_1_repository_1_1_organizations" ],
    [ "People", "namespace_organizations_manager_1_1_repository_1_1_people.html", "namespace_organizations_manager_1_1_repository_1_1_people" ],
    [ "Vehicles", "namespace_organizations_manager_1_1_repository_1_1_vehicles.html", "namespace_organizations_manager_1_1_repository_1_1_vehicles" ],
    [ "IRepository", "interface_organizations_manager_1_1_repository_1_1_i_repository.html", "interface_organizations_manager_1_1_repository_1_1_i_repository" ],
    [ "Repository", "class_organizations_manager_1_1_repository_1_1_repository.html", "class_organizations_manager_1_1_repository_1_1_repository" ],
    [ "RepositoryFactory", "class_organizations_manager_1_1_repository_1_1_repository_factory.html", "class_organizations_manager_1_1_repository_1_1_repository_factory" ]
];