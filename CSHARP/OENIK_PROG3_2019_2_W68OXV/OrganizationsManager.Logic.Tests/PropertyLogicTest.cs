﻿// <copyright file="PropertyLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using OrganizationsManager.Data;
    using OrganizationsManager.Logic.Buildings;
    using OrganizationsManager.Repository.Buildings;
    using OrganizationsManager.Repository.Organizations;

    /// <summary>
    /// Property logic test class.
    /// </summary>
    [TestFixture]
    internal class PropertyLogicTest
    {
        private Mock<ILogicFactory> logicFactory;
        private Mock<IPropertyRepository> propertyRepository;
        private IPropertyLogic propertyLogic;

        /// <summary>
        /// Setup method.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.logicFactory = new Mock<ILogicFactory>();
            this.propertyRepository = new Mock<IPropertyRepository>();
            this.logicFactory.Setup(f => f.GetPropertyLogic()).Returns(new PropertyLogic(this.propertyRepository.Object));
            this.propertyLogic = this.logicFactory.Object.GetPropertyLogic();
        }

        /// <summary>
        /// Tests the get all method.
        /// </summary>
        [Test]
        public void TestGetAll()
        {
            // Arrange
            this.propertyRepository.Setup(f => f.GetAll())
                                   .Returns(this.GetProperties());

            // Act
            var result = this.propertyLogic.GetAll();

            // Assert
            Assert.That(result, Is.EqualTo(this.GetProperties()));
        }

        /// <summary>
        /// Test the get one method.
        /// </summary>
        /// <param name="id">the index to request.</param>
        public void TestGetOne([Values(1, 2)] int id)
        {
            // Arrange
            this.propertyRepository.Setup(f => f.GetOne(It.IsAny<int>()))
                                   .Returns(this.GetProperties().FirstOrDefault(l => l.Id == id));

            // Act
            var result = this.propertyLogic.Get(id);

            // Assert
            Assert.That(result, Is.EqualTo(this.GetProperties().FirstOrDefault(f => f.Id == id)));
            this.propertyRepository.Verify(f => f.GetOne(It.Is<int>(l => l == id)), Times.Once());
        }

        /// <summary>
        /// Tests the add method.
        /// </summary>
        [Test]
        public void TestAdd()
        {
            // Arrange
            this.propertyRepository.Setup(f => f.Add(It.IsAny<Property>())).Verifiable();

            // Act
            this.propertyLogic.Add(new Property());

            // Assert
            this.propertyRepository.Verify(f => f.Add(It.IsAny<Property>()), Times.Once);
        }

        /// <summary>
        /// Tests the delete method.
        /// </summary>
        [Test]
        public void TestDelete()
        {
            // Arrange
            this.propertyRepository.Setup(f => f.Delete(It.IsAny<int>())).Returns(true);

            // Act
            var result = this.propertyLogic.Delete(1);
            Assert.IsTrue(result);

            this.propertyRepository.Verify(f => f.Delete(It.Is<int>(v => v == 1)), Times.Once);
        }

        /// <summary>
        /// Tests the update method.
        /// </summary>
        [Test]
        public void TestUpdate()
        {
            // Arrange
            this.propertyRepository.Setup(f => f.Update(It.IsAny<Property>(), It.IsAny<int>()))
                                   .Returns(true)
                                   .Verifiable();

            // Act
            var result = this.propertyLogic.Update(1, new Property());

            // Assert
            Assert.IsTrue(result);
            this.propertyRepository.Verify(
                l => l.Update(It.IsAny<Property>(), It.IsAny<int>()),
                Times.Once);
        }

        private IQueryable<Property> GetProperties()
        {
            return new List<Property>
            {
            new Property
            {
                Id = 1,
                BuildingSize = 500,
                City = "asd",
            },
            new Property
            {
                Id = 2,
                BuildingSize = 2000,
                City = "vau",
            },
            }.AsQueryable();
        }
    }
}
