﻿// <copyright file="VehicleLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using OrganizationsManager.Data;
    using OrganizationsManager.Logic.Vehicles;
    using OrganizationsManager.Repository.DTOS;
    using OrganizationsManager.Repository.Vehicles;

    /// <summary>
    /// Vehicle logic tests.
    /// </summary>
    [TestFixture]
    internal class VehicleLogicTest
    {
        private Mock<ILogicFactory> factory;
        private Mock<IVehicleRepository> vehicleRepository;
        private IVehicleLogic vehicleLogic;

        /// <summary>
        /// Creates the mocked logicFactory and repository and setups methods for them.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.factory = new Mock<ILogicFactory>();
            this.vehicleRepository = new Mock<IVehicleRepository>();
            this.factory.Setup(f => f.GetVehicleLogic()).Returns(new VehicleLogic(this.vehicleRepository.Object));
            this.vehicleRepository.Setup(r => r.GetAll()).Returns(this.GetVehicles());
            this.vehicleLogic = this.factory.Object.GetVehicleLogic();
        }

        /// <summary>
        /// Tests the delete method.
        /// </summary>
        [Test]
        public void TestDelete()
        {
            // Arrange
            this.vehicleRepository.Setup(f => f.Delete(It.IsAny<string>())).Returns(true);

            // Act
            bool result = this.vehicleLogic.Delete("asd123");

            // Assert
            Assert.That(result, Is.True);
            this.vehicleRepository.Verify(f => f.Delete(It.Is<string>(o => o.Equals("asd123"))), Times.Once);
        }

        /// <summary>
        /// Tests the groupedTravelDistance method.
        /// </summary>
        [Test]
        public void TestGetGroupedVehicleDistanceTravelled()
        {
            // Arrange
            this.vehicleRepository.Setup(f => f.GetGroupedVehicleDistances())
                .Returns(this.GetVehicles()
                             .GroupBy(l => l.OwnerId)
                             .Select(a => new GroupedVehicleTravelDistanceDTO { OrganizationId = a.Key, TotalDistance = a.Sum(f => f.DistanceTravelled) }));

            // Act
            var result = this.vehicleLogic.GetTotalDistance();

            // Assert
            Assert.That(result, Is.EqualTo(this.GetVehicles()
                             .GroupBy(l => l.OwnerId)
                             .Select(a => new GroupedVehicleTravelDistanceDTO { OrganizationId = a.Key, TotalDistance = a.Sum(f => f.DistanceTravelled) }.ToString())));
            this.vehicleRepository.Verify(l => l.GetGroupedVehicleDistances(), Times.Once);
        }

        /// <summary>
        /// Tests the add method.
        /// </summary>
        [Test]
        public void TestAdd()
        {
            // Arrange
            var veh = new Vehicle { LicensePlate = "qtya123", DistanceTravelled = 5000, IsRental = 0 };
            this.vehicleRepository.Setup(f => f.Add(It.IsAny<Vehicle>()));

            // Act
            this.vehicleLogic.Add(veh);

            // Assert
            this.vehicleRepository.Verify(f => f.Add(It.IsAny<Vehicle>()), Times.Once);
        }

        /// <summary>
        /// Tests the update method.
        /// </summary>
        [Test]
        public void TestUpdate()
        {
            // Arrange
            var veh = new Vehicle { LicensePlate = "qtya123", DistanceTravelled = 5000, IsRental = 0 };
            this.vehicleRepository.Setup(v => v.Update(It.IsAny<Vehicle>(), It.IsAny<string>())).Returns(true);

            // Act
            bool result = this.vehicleLogic.Update("asd123", veh);
            Assert.That(result, Is.True);
            this.vehicleRepository.Verify(l => l.Update(It.IsAny<Vehicle>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Tests the getAll method.
        /// </summary>
        [Test]
        public void TestGetAll()
        {
            // arrange
            this.vehicleRepository.Setup(r => r.GetAll()).Returns(this.GetVehicles());
            var logic = this.factory.Object.GetVehicleLogic();

            // act
            var result = logic.GetAll();

            // Assert
            Assert.That(result, Is.EqualTo(this.GetVehicles()));
            this.vehicleRepository.Verify(f => f.GetAll(), Times.Once);
        }

        private IQueryable<Vehicle> GetVehicles() => new List<Vehicle>
        {
            new Vehicle { LicensePlate = "asd123", Make = "audi", Model = "q8", DistanceTravelled = 50000 },
            new Vehicle { LicensePlate = "def456", Make = "bmw", Model = "320d", DistanceTravelled = 20000 },
        }.AsQueryable();
    }
}
