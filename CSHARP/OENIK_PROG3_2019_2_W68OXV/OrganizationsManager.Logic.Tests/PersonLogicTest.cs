﻿// <copyright file="PersonLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOs;
    using OrganizationsManager.Repository.People;

    /// <summary>
    /// Person logic test class.
    /// </summary>
    [TestFixture]
    internal class PersonLogicTest
    {
        private Mock<IPersonRepository> personRepo;
        private Mock<ILogicFactory> logicFactory;
        private IPersonLogic personLogic;

        /// <summary>
        /// Set up method.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.personRepo = new Mock<IPersonRepository>();
            this.logicFactory = new Mock<ILogicFactory>();
            this.logicFactory.Setup(f => f.GetPersonLogic()).Returns(new PersonLogic(this.personRepo.Object));
            this.personLogic = this.logicFactory.Object.GetPersonLogic();
        }

        /// <summary>
        /// Tests the get all method.
        /// </summary>
        [Test]
        public void TestGetAll()
        {
            // Arrange
            this.personRepo.Setup(f => f.GetAll()).Returns(this.GetPeople()).Verifiable();

            // Act
            var res = this.personLogic.GetAll();

            // Assert
            Assert.That(res, Is.EqualTo(this.GetPeople()));
            this.personRepo.Verify(f => f.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the get one method.
        /// </summary>
        /// <param name="id">Primary key.</param>
        [Test]
        public void TestGetOne([Values(1, 2)]int id)
        {
            this.personRepo.Setup(f => f.GetOne(It.IsAny<int>())).Returns(this.GetPeople().FirstOrDefault(f => f.Id == id)).Verifiable();

            var result = this.personLogic.Get(id);

            Assert.That(result, Is.EqualTo(this.GetPeople().FirstOrDefault(l => l.Id == id)));
            this.personRepo.Verify(l => l.GetOne(It.Is<int>(val => val == id)), Times.Once);
        }

        /// <summary>
        /// Test the update method.
        /// </summary>
        [Test]
        public void TestUpdate()
        {
            this.personRepo.Setup(l => l.Update(It.IsAny<Person>(), It.IsAny<int>()))
                           .Returns(true)
                           .Verifiable();
            var result = this.personLogic.Update(1, new Person());

            Assert.IsTrue(result);
            this.personRepo.Verify(l => l.Update(It.IsAny<Person>(), It.Is<int>(f => f == 1)), Times.Once);
        }

        /// <summary>
        /// Tests the delete method.
        /// </summary>
        [Test]
        public void TestDelete()
        {
            this.personRepo.Setup(l => l.Delete(It.IsAny<int>())).Returns(true).Verifiable();

            var result = this.personLogic.Delete(1);

            this.personRepo.Verify(l => l.Delete(It.Is<int>(d => d == 1)), Times.Once);
        }

        /// <summary>
        /// Tests the average age method.
        /// </summary>
        [Test]
        public void TestAverageAge()
        {
            this.personRepo.Setup(l => l.GetAveragePeopleAgePerOrganization()).Returns(new List<AveragePersonAgeDTO>()).Verifiable();

            var res = this.personLogic.GetAveragePersonAge();
            Assert.That(res, Is.EqualTo(new List<string>().AsEnumerable()));

            this.personRepo.Verify(k => k.GetAveragePeopleAgePerOrganization(), Times.Once);
        }

        /// <summary>
        /// Test the add method.
        /// </summary>
        [Test]
        public void TestAdd()
        {
            this.personRepo.Setup(p => p.Add(It.IsAny<Person>())).Verifiable();

            this.personLogic.Add(new Person());

            this.personRepo.Verify(p => p.Add(It.IsAny<Person>()), Times.Once);
        }

        private IQueryable<Person> GetPeople()
        {
            return new List<Person>
            {
                new Person { Id = 1, PersonName = "teknokol jozsef" },
                new Person { Id = 2, PersonName = "Valvolgyi noemi" },
            }.AsQueryable();
        }
    }
}
