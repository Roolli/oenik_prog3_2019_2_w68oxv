﻿// <copyright file="JavaLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using OrganizationsManager.Logic.JavaInteraction;
    using OrganizationsManager.Repository.Buildings;
    using OrganizationsManager.Repository.Vehicles;

    /// <summary>
    /// Java logic test class.
    /// </summary>
    [TestFixture]
    internal class JavaLogicTest
    {
        private IJavaLogic javaLogic;
        private Mock<HttpMessageHandler> httpMessageHandler;

        /// <summary>
        /// Set up method.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.httpMessageHandler = new Mock<HttpMessageHandler>();
            this.httpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.Is<HttpRequestMessage>(v => v.Method == HttpMethod.Get),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage() { Content = new StringContent("{'res':['ok','boomer']}"), StatusCode = System.Net.HttpStatusCode.OK }).Verifiable();

            var logicFactory = new Mock<ILogicFactory>();
            var mockedClient = new HttpClient(this.httpMessageHandler.Object);
            logicFactory.Setup(f => f.GetJavaLogic()).Returns(new JavaLogic(mockedClient));
            this.javaLogic = logicFactory.Object.GetJavaLogic();
        }

        /// <summary>
        /// Test the property estimation function.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [Test]
        public async Task TestPropertyEstimation()
        {
            // Act
            var result = await this.javaLogic.GetEstimatedPropertyValue(new List<PropertyDTO>());

            // Assert
            Assert.That(result, Is.EqualTo($"ok{Environment.NewLine}boomer"));

            // for some reason protected mock verification is not working even though it returns the correct value
        }

        /// <summary>
        /// Tests the vehicle estimation function.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [Test]
        public async Task TestVehicleEstimation()
        {
            // Act
            var result = await this.javaLogic.GetEstimatedVehicleValue(new List<VehicleDTO>());

            // Assert
            Assert.That(result, Is.EqualTo($"ok{Environment.NewLine}boomer"));

            // for some reason protected mock verification is not working even though it returns the correct value
        }
    }
}
