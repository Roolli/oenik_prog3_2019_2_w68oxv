﻿// <copyright file="OrganizationLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.Organizations;

    /// <summary>
    /// Organization Logic Test class.
    /// </summary>
    [TestFixture]
    public class OrganizationLogicTest
    {
        private Mock<ILogicFactory> logicFactory;
        private Mock<IOrganizationRepository> organizationRepository;

        /// <summary>
        /// Creates the Mocked repository and the logic classes.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.logicFactory = new Mock<ILogicFactory>();
            this.organizationRepository = new Mock<IOrganizationRepository>();
            this.logicFactory.Setup(l => l.GetOrganizationLogic()).Returns(new OrganizationLogic(this.organizationRepository.Object));
            this.organizationRepository.Setup(l => l.Delete(It.IsAny<int>())).Returns(true).Verifiable();
        }

        /// <summary>
        /// Tests the update method.
        /// </summary>
        /// <param name="value">The supplied values to test for.</param>
        [Test]
        [Sequential]
        public void TestUpdate([Values(1, 2, 3)]int value)
        {
            // Arrange
            this.organizationRepository.Setup(l => l.Update(It.IsAny<Organization>(), It.IsAny<int>())).Returns(true).Verifiable();
            var logic = this.logicFactory.Object.GetOrganizationLogic();
            var data = this.GetOrganizations();

            // Act & Assert
            Assert.That(logic.Update(value, new Organization { Name = "Mészáros and co." }), Is.True);
            this.organizationRepository.Verify(f => f.Update(It.IsAny<Organization>(), It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests the add method.
        /// </summary>
        [Test]
        public void TestAdd()
        {
            // Arrange
            var organizationLogic = this.logicFactory.Object.GetOrganizationLogic();
            this.organizationRepository.Setup(l => l.Add(It.IsAny<Organization>())).Verifiable();

            var organization = new Organization
            {
                FoundationDate = DateTime.UtcNow,
                GroupType = "nonprofit",
                Id = 1,
                MemberCount = 0,
                Name = "Asd",
                Vault = 0,
            };

            // Act
            organizationLogic.Add(organization);

            // Assert
            this.organizationRepository.Verify(r => r.Add(It.IsAny<Organization>()), Times.Once);
        }

        /// <summary>J
        /// Test the Get all method.
        /// </summary>
        [Test]
        public void TestGetAll()
        {
            // Arrange
            var organizationLogic = this.logicFactory.Object.GetOrganizationLogic();
            this.organizationRepository.Setup(l => l.GetAll()).Returns(this.GetOrganizations()).Verifiable();

            // Act
            var result = organizationLogic.GetAll();

            // Assert
            Assert.That(result.Count(), Is.EqualTo(this.GetOrganizations().Count()));
            this.organizationRepository.Verify(t => t.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the getOne CRUD method.
        /// </summary>
        [Test]

        public void GetOne()
        {
            this.organizationRepository.Setup(l => l.GetOne(It.IsAny<int>())).Returns(this.GetOrganizations().First()).Verifiable();
            var logic = this.logicFactory.Object.GetOrganizationLogic();

            var organization = logic.Get(5);
            var expected = this.GetOrganizations().First();

            Assert.AreEqual(expected, organization);
            this.organizationRepository.Verify(v => v.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// GroupedWealths test.
        /// </summary>
        [Test]
        public void TestGetGroupedWealths()
        {
            // Arrange
            var organizationLogic = this.logicFactory.Object.GetOrganizationLogic();
            this.organizationRepository.Setup(f => f.GetGroupedWealths()).Returns(this.GetOrganizations().GroupBy(v => v.GroupType).Select(f => new Repository.DTOS.GroupedWealthDTO
            {
                OrganizationType = f.Key,
                WealthCount = f.Sum(p => p.Vault),
            }));

            // Act
            var result = organizationLogic.GetGroupedWealths();

            // Assert
            CollectionAssert.AreEquivalent(result, this.GetOrganizations().GroupBy(v => v.GroupType).Select(f => new Repository.DTOS.GroupedWealthDTO
            {
                OrganizationType = f.Key,
                WealthCount = f.Sum(p => p.Vault),
            }).Select(f => f.ToString()));
            this.organizationRepository.Verify(l => l.GetGroupedWealths(), Times.Once);
        }

        private IQueryable<Organization> GetOrganizations() => new List<Organization>
        {
         new Organization { FoundationDate = DateTime.UtcNow, Id = 1, Name = "asdas", GroupType = "nonProfit", Vault = 5000 },
         new Organization { Id = 2, Name = "fwijaiwfjwalif", Vault = 5000, GroupType = "nonProfit" },
         new Organization { Id = 3, Name = "fwiajfwlijfawilfjawlifjaw", Vault = 50000, GroupType = "goverment" },
        }.AsQueryable();
    }
}
