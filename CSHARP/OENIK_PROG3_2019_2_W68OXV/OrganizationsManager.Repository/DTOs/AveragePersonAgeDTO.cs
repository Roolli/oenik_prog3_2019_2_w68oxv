﻿// <copyright file="AveragePersonAgeDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.DTOs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A data transfer object for tranferring the average age per organization.
    /// </summary>
    public class AveragePersonAgeDTO
    {
        /// <summary>
        /// Gets or sets the average age for the given organization.
        /// </summary>
        public double AverageAge { get; set; }

        /// <summary>
        /// Gets or sets the organization's name.
        /// </summary>
        public string OrganizationName { get; set; }

        /// <inheritdoc/>
        public override string ToString() => $"Organization name: {this.OrganizationName} Average age:{this.AverageAge:##.##}";

        /// <inheritdoc/>
        public override bool Equals(object obj) => obj is AveragePersonAgeDTO dTO && this.AverageAge == dTO.AverageAge && this.OrganizationName == dTO.OrganizationName;

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            var hashCode = -1990125764;
            hashCode = (hashCode * -1521134295) + this.AverageAge.GetHashCode();
            hashCode = (hashCode * -1521134295) + EqualityComparer<string>.Default.GetHashCode(this.OrganizationName);
            return hashCode;
        }
    }
}
