﻿// <copyright file="GroupedVehicleTravelDistanceDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.DTOS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// DTO object for gather.
    /// </summary>
    public class GroupedVehicleTravelDistanceDTO
    {
        /// <summary>
        /// Gets or sets the Organization's Id.
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the total distance driven from all cars for the given organization.
        /// </summary>
        public int TotalDistance { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj) => obj is GroupedVehicleTravelDistanceDTO dTO && this.OrganizationId == dTO.OrganizationId && this.TotalDistance == dTO.TotalDistance;

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            var hashCode = 237763865;
            hashCode = (hashCode * -1521134295) + this.OrganizationId.GetHashCode();
            hashCode = (hashCode * -1521134295) + this.TotalDistance.GetHashCode();
            return hashCode;
        }

        /// <inheritdoc/>
        public override string ToString() => $"Organization:{this.OrganizationId} DistanceTravelled:{this.TotalDistance}";
    }
}
