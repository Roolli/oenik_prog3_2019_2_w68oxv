﻿// <copyright file="GroupedWealthDTO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.DTOS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data transfer object for transferring grouped wealths.
    /// </summary>
    public class GroupedWealthDTO
    {
        /// <summary>
        /// Gets or sets the organization's type.
        /// </summary>
        public string OrganizationType { get; set; }

        /// <summary>
        /// Gets or sets the wealth for the organization.
        /// </summary>
        public int WealthCount { get; set; }

        /// <summary>
        /// String override.
        /// </summary>
        /// <returns>magic.</returns>
        public override string ToString() => $"{this.OrganizationType}:{this.WealthCount}";
    }
}
