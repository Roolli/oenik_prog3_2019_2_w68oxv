﻿// <copyright file="IOrganizationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Organizations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOS;

    /// <summary>
    /// All non CRUD operations for the Organization Repository.
    /// </summary>
    public interface IOrganizationRepository : IRepository<Organization>
    {
        /// <summary>
        /// Gets all the organization's vehicles' total distance travelled.
        /// </summary>
        /// <returns>The collection of all the organizations and their vehicles' total distance travelled.</returns>
        IEnumerable<GroupedWealthDTO> GetGroupedWealths();
    }
}
