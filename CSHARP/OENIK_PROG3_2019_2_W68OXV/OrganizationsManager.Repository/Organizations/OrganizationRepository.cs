﻿// <copyright file="OrganizationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Organizations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOS;
    using OrganizationsManager.Repository.Organizations;

    /// <summary>
    /// Repository for handling all organization based operations.
    /// </summary>
    internal class OrganizationRepository : Repository<Organization>, IOrganizationRepository
    {
        /// <inheritdoc/>
        public IEnumerable<GroupedWealthDTO> GetGroupedWealths()
        {
            using (var ctx = new OrganizationsContext())
            {
                return ctx.Set<Organization>().GroupBy(v => v.GroupType).Select(f => new GroupedWealthDTO { OrganizationType = f.Key, WealthCount = f.Sum(l => l.Vault) }).ToList();
            }
        }
    }
}
