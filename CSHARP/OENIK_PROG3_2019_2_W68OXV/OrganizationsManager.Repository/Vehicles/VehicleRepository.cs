﻿// <copyright file="VehicleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Vehicles
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOS;

    /// <summary>
    /// Repository for handling the Vehicle Entities.
    /// </summary>
    internal class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        /// <inheritdoc/>
        public IEnumerable<GroupedVehicleTravelDistanceDTO> GetGroupedVehicleDistances()
        {
            using (var ctx = new OrganizationsContext())
            {
                return ctx.Set<Vehicle>().Include(l => l.Organization).GroupBy(v => v.Organization.Id).Select(g => new GroupedVehicleTravelDistanceDTO { OrganizationId = g.Key, TotalDistance = g.Sum(p => p.DistanceTravelled) }).ToList();
            }
        }
    }
}
