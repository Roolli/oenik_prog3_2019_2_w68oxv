﻿// <copyright file="IVehicleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Vehicles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOS;

    /// <summary>
    /// Definition for all NON-CRUD methods for this repository.
    /// </summary>
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        /// <summary>
        /// Returns the total distance travelled of all vehicles per Organization.
        /// </summary>
        /// <returns>Collection of organizations and their total travelled distance by their vehicles.</returns>
        IEnumerable<GroupedVehicleTravelDistanceDTO> GetGroupedVehicleDistances();
    }
}
