﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// base requirements for all repository objects.
    /// </summary>
    /// <typeparam name="T"> The type of repository you wish to create.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets all entities of the T Type.
        /// </summary>
        /// <returns> A collection of all entities.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets an entity with the specified ID or null of none found.
        /// </summary>
        /// <param name="keyValues">Unique identifiers for the entity.</param>
        /// <returns>The entity with the specified ID or null.</returns>
        T GetOne(params object[] keyValues);

        /// <summary>
        /// Adds one entity into the database.
        /// </summary>
        /// <param name="entity">Entity to add.</param>
        void Add(T entity);

        /// <summary>
        /// Updates the given entity.
        /// </summary>
        /// <param name="entity">The updated entity.</param>
        /// <param name="id"> the id of the entity.</param>
        /// <returns> true on success,false otherwise.</returns>
        bool Update(T entity, object id);

        /// <summary>
        /// Deletes the entity with the given key.
        /// </summary>
        /// <param name="id">Unique Identifier for the given entity.</param>
        /// <returns> true on successful delete false otherwise. </returns>
        bool Delete(object id);
    }
}
