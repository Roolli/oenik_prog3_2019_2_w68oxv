﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Base class for all Repositories where all basic CRUD operations are implemented.
    /// </summary>
    /// <typeparam name="T">Type of entity the repository manages.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Inserts the entity into the database.
        /// </summary>
        /// <param name="entity">Entity to insert into the database.</param>
        public void Add(T entity)
        {
            using (var ctx = new OrganizationsContext())
            {
                ctx.Set<T>().Add(entity);
                ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes the entity with the specified Index.
        /// </summary>
        /// <param name="id">The primary key for entity.</param>
        /// <returns>true on successful delete false otherwise.</returns>
        public bool Delete(object id)
        {
            using (var ctx = new OrganizationsContext())
            {
                T entity = ctx.Set<T>().Find(id);
                if (entity is null)
                {
                    return false;
                }

                ctx.Set<T>().Remove(entity);
                ctx.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Gets the collection of entities.
        /// </summary>
        /// <returns> A collection containing all the entities of the type. </returns>
        public IQueryable<T> GetAll()
        {
            using (var ctx = new OrganizationsContext())
            {
                return ctx.Set<T>().ToList().AsQueryable();
            }
        }

        /// <summary>
        /// Gets the entity with the specified ID.
        /// </summary>
        /// <param name="keyValues">ID of the entity.</param>
        /// <returns>The entity if found, null otherwise.</returns>
        public T GetOne(params object[] keyValues)
        {
            using (var ctx = new OrganizationsContext())
            {
                return ctx.Set<T>().Find(keyValues);
            }
        }

        /// <summary>
        /// Updates the entity with the given key.
        /// </summary>
        /// <param name="entity">The updated entity.</param>
        /// <param name="id">The entity's key to update.</param>
        /// <returns>true on success, false otherwise.</returns>
        public bool Update(T entity, object id)
        {
            using (var ctx = new OrganizationsContext())
            {
                var entityInDb = ctx.Set<T>().Find(id);
                if (entityInDb is null)
                {
                    return false;
                }

                ctx.Entry(entityInDb).CurrentValues.SetValues(entity);
                ctx.SaveChanges();
                return true;
            }
        }
    }
}
