﻿// <copyright file="IPropertyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Buildings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Defines all methods the property repository needs to implement.
    /// </summary>
    public interface IPropertyRepository : IRepository<Property>
    {
    }
}
