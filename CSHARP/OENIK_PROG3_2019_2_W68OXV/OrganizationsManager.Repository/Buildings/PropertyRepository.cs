﻿// <copyright file="PropertyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.Buildings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;

    /// <summary>
    /// Contains all the repository methods for the property entity.
    /// </summary>
    internal class PropertyRepository : Repository<Property>, IPropertyRepository
    {
    }
}
