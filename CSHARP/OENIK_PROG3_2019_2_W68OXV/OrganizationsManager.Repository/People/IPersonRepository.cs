﻿// <copyright file="IPersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.People
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOs;

    /// <summary>
    /// Defines all methods for the Person Repository class.
    /// </summary>
    public interface IPersonRepository : IRepository<Person>
    {
        /// <summary>
        /// Gets the average age for all organizations.
        /// </summary>
        /// <returns>A collection  of objects which contains the average person age along with the organization's name.</returns>
        IEnumerable<AveragePersonAgeDTO> GetAveragePeopleAgePerOrganization();
    }
}
