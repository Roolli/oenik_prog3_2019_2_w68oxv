﻿// <copyright file="PersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository.People
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.DTOs;

    /// <summary>
    /// Repository class for managing the person entities.
    /// </summary>
    internal class PersonRepository : Repository<Person>, IPersonRepository
    {
        /// <summary>
        /// Gets the average person age for every organization.
        /// </summary>
        /// <returns> A collection of DTO objects containing the average age and the organization's name.</returns>
        public IEnumerable<AveragePersonAgeDTO> GetAveragePeopleAgePerOrganization()
        {
            using (var ctx = new OrganizationsContext())
            {
                return ctx.Set<Person>()
                          .Include(l => l.Organization)
                          .GroupBy(f => f.Organization.Name)
                          .Select(f => new AveragePersonAgeDTO { AverageAge = f.Average(v => Math.Abs(DbFunctions.DiffYears(v.DateOfBirth, DateTime.UtcNow).Value)), OrganizationName = f.Key.ToString() })
                          .ToList();
            }
        }
    }
}
