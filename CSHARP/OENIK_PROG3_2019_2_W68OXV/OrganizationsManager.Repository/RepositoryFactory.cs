﻿// <copyright file="RepositoryFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrganizationsManager.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using OrganizationsManager.Data;
    using OrganizationsManager.Repository.Buildings;
    using OrganizationsManager.Repository.Organizations;
    using OrganizationsManager.Repository.People;
    using OrganizationsManager.Repository.Vehicles;

    /// <summary>
    /// A factory for creating Repositories.
    /// </summary>
    public class RepositoryFactory
    {
        /// <summary>
        /// Creates a new Organization Repository.
        /// </summary>
        /// <returns>A new  OrganizationRepository instance.</returns>
        public IOrganizationRepository GetOrganizationRepository() => new OrganizationRepository();

        /// <summary>
        /// Creates a new Vehicle Repository.
        /// </summary>
        /// <returns>A new VehicleRepository instance.</returns>
        public IVehicleRepository GetVehicleRepository() => new VehicleRepository();

        /// <summary>
        /// Creates a new Person Repository.
        /// </summary>
        /// <returns>A new PersonRepository instance.</returns>
        public IPersonRepository GetPersonRepository() => new PersonRepository();

        /// <summary>
        /// Creates a new Property repository.
        /// </summary>
        /// <returns> a new instance of <see cref="PropertyRepository"/>.</returns>
        public IPropertyRepository GetPropertyRepository() => new PropertyRepository();
    }
}
