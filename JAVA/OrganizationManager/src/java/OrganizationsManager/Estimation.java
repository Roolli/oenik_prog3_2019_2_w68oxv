/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrganizationsManager;

import java.util.Random;

/**
 *
 * @author Rooll
 */
public class Estimation {
    final static Random r = new Random();
    public static String estimateVehicle(VehicleDTO veh)
    {
       float finalPrice = 0;
       int modelLength =  veh.getModel().length();
       int makeLength = veh.getMake().length();
       int distanceTravelled = veh.getDistanceTravelled();
       if(distanceTravelled > 0 && distanceTravelled <= 50000)
       {
        finalPrice = (2500000 - distanceTravelled * (r.nextFloat() * r.nextInt(modelLength)));
       }
       else if(distanceTravelled >  50000 && distanceTravelled <= 100000)
       {
           finalPrice = (2000000 - distanceTravelled * (r.nextFloat() * r.nextInt(makeLength)));
       }
       else {
           finalPrice = (100000 - distanceTravelled * (r.nextInt(modelLength)));
       }
       return String.format("The following vehicle: %s %s could be sold for %.2f",veh.getMake(),veh.getModel(),finalPrice);
    }
    public static String estimateBuilding(BuildingDTO building)
    {
        float finalPrice = 0;
        int buildingSize = building.getBuildingSize();
        if(buildingSize >= 5000 && buildingSize <= 10000)
        {
            finalPrice = buildingSize * 1000000 * r.nextFloat();
        }
        else if(buildingSize > 10000 && buildingSize <= 50000)
        {
            finalPrice = buildingSize * 2000000 * r.nextFloat();
        }
        else {
            finalPrice = buildingSize * 5000000 * r.nextFloat();
        }
        return String.format("The building which is located in:%s could be sold for:%.2f", building.getCity(),finalPrice);
    }
}
