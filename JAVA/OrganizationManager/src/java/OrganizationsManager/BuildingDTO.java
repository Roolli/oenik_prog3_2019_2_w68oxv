/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrganizationsManager;

/**
 *
 * @author Rooll
 */
public class BuildingDTO {
    public static final String BUILDING_JSON_NAME = "PropertyJson";
  private int buildingSize;
  private String city;

    public BuildingDTO(int buildingSize, String city) {
        this.buildingSize = buildingSize;
        this.city = city;
    }

    public int getBuildingSize() {
        return buildingSize;
    }

    public void setBuildingSize(int buildingSize) {
        this.buildingSize = buildingSize;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
}
