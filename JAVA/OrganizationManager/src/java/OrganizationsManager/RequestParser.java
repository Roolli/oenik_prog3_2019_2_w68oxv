/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrganizationsManager;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author Rooll
 */
public class RequestParser {
    public static List<VehicleDTO> parseVehicles(String json)
    {
        JsonReader reader = Json.createReader(new StringReader(json));
        JsonArray values  = reader.readArray();
        
         List<VehicleDTO> vehicles = new ArrayList<>();
         
        for (int i = 0; i < values.size(); i++) {
            JsonObject vehObject = values.getJsonObject(i);
         VehicleDTO veh =  new VehicleDTO(vehObject.getInt("DistanceTravelled"),vehObject.getString("Make"),vehObject.getString("Model"));
         vehicles.add(veh);
        }
        return vehicles;
    }
    public static List<BuildingDTO> parseBuildings(String json)
    {
    JsonReader reader = Json.createReader(new StringReader(json));
    JsonArray values = reader.readArray();
    List<BuildingDTO> buildings = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            JsonObject obj = values.getJsonObject(i);
         BuildingDTO building =   new BuildingDTO(obj.getInt("BuildingSize"), obj.getString("City"));
         buildings.add(building);
        }
    return buildings;
    }
}
