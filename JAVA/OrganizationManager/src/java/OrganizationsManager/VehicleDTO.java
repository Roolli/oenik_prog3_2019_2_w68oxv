/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrganizationsManager;

import java.util.Date;

/**
 *
 * @author Rooll
 */
public class VehicleDTO {
    public static  final String VEHICLE_JSON_NAME = "VehicleJson";
    
   private int distanceTravelled;
   private String make;
   private String model;
    public VehicleDTO(int distanceTravelled, String make, String model) {
        this.distanceTravelled = distanceTravelled;
        this.make = make;
        this.model = model;
    }

   
   
    public int getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(int distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
