/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OrganizationsManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rooll
 */
@WebServlet(name = "EstimationHandler",urlPatterns = {"/EstimationHandler"})
public class EstimationHandler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.processRequest(request, response);
         PrintWriter pw = response.getWriter();
          JsonObjectBuilder job = Json.createObjectBuilder();
           JsonArrayBuilder jab = Json.createArrayBuilder();
        if(request.getParameter(VehicleDTO.VEHICLE_JSON_NAME) != null)
        {
          List<VehicleDTO> vehicles = RequestParser.parseVehicles(request.getParameter(VehicleDTO.VEHICLE_JSON_NAME));
          for(VehicleDTO veh : vehicles)
          {
              jab.add(Estimation.estimateVehicle(veh));
          }
          job.add("res",jab);
          String res = job.build().toString();
          pw.println(res);
        }
        else if(request.getParameter(BuildingDTO.BUILDING_JSON_NAME)!= null)
        {
            
            List<BuildingDTO> buildings = RequestParser.parseBuildings(request.getParameter(BuildingDTO.BUILDING_JSON_NAME));
           
            for(BuildingDTO building : buildings)
            {
                jab.add(Estimation.estimateBuilding(building));
            }
            job.add("res",jab);
            pw.println(job.build().toString());
        }
        else {
            job.add("res", "Something went wrong :/");
        pw.println(job.build().toString());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
