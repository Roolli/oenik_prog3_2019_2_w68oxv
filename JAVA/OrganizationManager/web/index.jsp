<%-- 
    Document   : index
    Created on : Oct 24, 2019, 6:42:27 PM
    Author     : Rooll
--%>

<%@page import="OrganizationsManager.VehicleDTO"%>
<%@page import="OrganizationsManager.BuildingDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%   
        if(request.getParameter(VehicleDTO.VEHICLE_JSON_NAME) == null && request.getParameter(BuildingDTO.BUILDING_JSON_NAME)== null)
        {
            out.println("no vaild arguments found");
        }
        else {
        ServletContext context = getServletContext();
        RequestDispatcher rd = context.getRequestDispatcher("/EstimationHandler");
        rd.forward(request, response);
        }
        %>
    </body>
</html>
